//
//  MySadiVC.swift
//  GoColorBlind
//
//  Created by Prashant Shinde on 11/11/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit

class MySadiVC: UIViewController {

    
    @IBOutlet weak var completeProfileTV: UITableView!
    @IBOutlet weak var premiumMatchesCV: UICollectionView!
    @IBOutlet weak var recentlyJoinedCV: UICollectionView!
    @IBOutlet weak var recentlyVisitorsCV: UICollectionView!
    @IBOutlet weak var termsBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func upgradeBtn(_ sender: Any) {
    }
    @IBAction func editProfileBtn(_ sender: Any) {
    }
    
    @IBAction func seeAllOne(_ sender: Any) {
        let premium = self.storyboard?.instantiateViewController(withIdentifier: "premiummatchvc") as! PremiumMatchVC
        premium.Str = "one"
         self.present(premium, animated: true, completion: nil)
    }
    
    @IBAction func seeAllTwo(_ sender: Any) {
        let premium = self.storyboard?.instantiateViewController(withIdentifier: "premiummatchvc") as! PremiumMatchVC
        premium.Str = "two"
        self.present(premium, animated: true, completion: nil)

    }
    @IBAction func seeAllThree(_ sender: Any) {
        let premium = self.storyboard?.instantiateViewController(withIdentifier: "premiummatchvc") as! PremiumMatchVC
        premium.Str = "three"
        self.present(premium, animated: true, completion: nil)

    }
    @IBAction func notificationBtn(_ sender: Any) {
        let premium = self.storyboard?.instantiateViewController(withIdentifier: "notificationvc") as! NotificationVC
        self.present(premium, animated: true, completion: nil)

    }
 
}

class ProfileCell : UITableViewCell
{
    @IBOutlet weak var profileBtn: UIButton!
    
}

class PremiumCell : UICollectionViewCell
{
    @IBOutlet weak var pImage: UIImageView!
    @IBOutlet weak var pYear: UILabel!
    @IBOutlet weak var pName: UILabel!
    @IBOutlet weak var pAddress: UILabel!
    @IBOutlet weak var pInterest: KGHighLightedButton!
    @IBOutlet weak var pFullName: UILabel!
    
}

class RecentlyJoinedCell : UICollectionViewCell
{
    @IBOutlet weak var rjImage: UIImageView!
    @IBOutlet weak var rjFullName: UILabel!
    @IBOutlet weak var rjYear: UILabel!
    @IBOutlet weak var rjName: UILabel!
    @IBOutlet weak var rjAddress: UILabel!
    @IBOutlet weak var rjInterest: KGHighLightedButton!
    
}

class RecentlyVisitorsCell : UICollectionViewCell
{
    @IBOutlet weak var rvImage: UIImageView!
    @IBOutlet weak var rvFullName: UILabel!
    @IBOutlet weak var rvYear: UILabel!
    @IBOutlet weak var rvName: UILabel!
    @IBOutlet weak var rvAddress: UILabel!
    @IBOutlet weak var rvInterest: KGHighLightedButton!
    
}

extension MySadiVC : UITableViewDataSource , UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ProfileCell = completeProfileTV.dequeueReusableCell(withIdentifier: "profilecell") as! ProfileCell
       
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
}


extension MySadiVC : UICollectionViewDataSource , UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(collectionView == premiumMatchesCV)
        {
            let cell : PremiumCell = premiumMatchesCV.dequeueReusableCell(withReuseIdentifier: "premiumcell", for: indexPath) as! PremiumCell
            // 102 204 255
            cell.pInterest.layer.borderWidth = 1.0
            cell.pInterest.layer.borderColor = UIColor.init(red: 102/255, green: 204/255, blue: 255/255, alpha: 1.0).cgColor
            return cell
        }
        else if(collectionView == recentlyJoinedCV)
        {
            let cell : RecentlyJoinedCell = recentlyJoinedCV.dequeueReusableCell(withReuseIdentifier: "recentlyjoinedcell", for: indexPath) as! RecentlyJoinedCell
            cell.rjInterest.layer.borderWidth = 1.0
            cell.rjInterest.layer.borderColor = UIColor.init(red: 102/255, green: 204/255, blue: 255/255, alpha: 1.0).cgColor
            return cell
        }
        else  
        {
            let cell : RecentlyVisitorsCell = recentlyVisitorsCV.dequeueReusableCell(withReuseIdentifier: "recentlyvisitorscell", for: indexPath) as! RecentlyVisitorsCell
            cell.rvInterest.layer.borderWidth = 1.0
            cell.rvInterest.layer.borderColor = UIColor.init(red: 102/255, green: 204/255, blue: 255/255, alpha: 1.0).cgColor
            return cell

        }
        
    }
    
}

