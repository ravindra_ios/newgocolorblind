//
//  RefineViewController.swift
//  GoColorBlind
//
//  Created by Ravi on 11/22/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit


class RefineViewController: UIViewController {

    @IBOutlet weak var lookingForBtn: UIButton!
    @IBOutlet weak var countryBtn: UIButton!
    @IBOutlet weak var listTableView: UITableView!
    @IBOutlet weak var downArrowOne: UIImageView!
    @IBOutlet weak var downArrowTwo: UIImageView!
    @IBOutlet weak var ageMinLbl: UILabel!
    @IBOutlet weak var ageMaxLbl: UILabel!
    @IBOutlet weak var minHtBtn: UIButton!
    @IBOutlet weak var maxHtBtn: UIButton!
    @IBOutlet weak var minHtImg: UIImageView!
    @IBOutlet weak var maxHtImg: UIImageView!
    @IBOutlet weak var HtView: UIView!
    @IBOutlet weak var lookingView: UIView!
    @IBOutlet weak var countryView: UIView!
    
    var heightList : [String] = []
    
    var check : String = ""
    var heightArr : [NSDictionary] = []
    var countryArr : [NSDictionary] = []
    var countryList : [String] = []
    let rangeSlider1 = RangeSlider(frame: CGRect.zero)
  //  let rangeSlider2 = RangeSlider(frame: CGRect.zero)
    
    var genderList : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lookingForBtn.contentEdgeInsets = UIEdgeInsets(top:0,left:10, bottom:0, right:0)
        countryBtn.contentEdgeInsets = UIEdgeInsets(top:0,left:10, bottom:0, right:0)
        minHtBtn.contentEdgeInsets = UIEdgeInsets(top:0,left:10, bottom:0, right:0)
        maxHtBtn.contentEdgeInsets = UIEdgeInsets(top:0,left:10, bottom:0, right:0)
      
        // Do any additional setup after loading the view, typically from a nib.
        rangeSlider1.trackHighlightTintColor = UIColor.init(red: 253.0/255, green: 89.0/255, blue: 95.0/255, alpha: 1.0)
     
        view.addSubview(rangeSlider1)
        rangeSlider1.addTarget(self, action: #selector(SliderViewController.rangeSliderValueChanged(_:)), for: .valueChanged)
        
        listTableView.isHidden = true
        self.genderList = ["None" , "Male" , "Female"]
        self.GetData(urlStr: "get_height", params: "", checkStr: "height")
        
        
    }

    override func viewDidLayoutSubviews() {
        let margin: CGFloat = 20.0
        let width = view.bounds.width - 2.0 * margin
        rangeSlider1.frame = CGRect(x: margin, y: margin + topLayoutGuide.length + 98,
                                    width: width, height: 28.0)
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    func rangeSliderValueChanged(_ rangeSlider: RangeSlider) {
        print("Range slider value changed: (\(rangeSlider.lowerValue) , \(rangeSlider.upperValue))")
        let minValue = Int(rangeSlider.lowerValue)
        ageMinLbl.text! = "\(minValue)"
        let maxValue = Int(rangeSlider.upperValue)
        ageMaxLbl.text! = "\(maxValue)"
    }
   
    
    @IBAction func btnLooking(_ sender: Any)
    {
        if listTableView.isHidden
        {
            listTableView.isHidden = false
            listTableView.frame.origin.x = lookingView.frame.origin.x + 8
            listTableView.frame.origin.y = lookingForBtn.frame.origin.y + 302
            listTableView.frame.size.height = lookingForBtn.frame.size.height + 118
            listTableView.frame.size.width = lookingForBtn.frame.size.width
            downArrowOne.image = UIImage(named : "up-arrow.png")
            minHtImg.image = UIImage(named : "arrow-down-sign-to-navigate.png")
            maxHtImg.image = UIImage(named : "arrow-down-sign-to-navigate.png")
            downArrowTwo.image = UIImage(named : "arrow-down-sign-to-navigate.png")
           
            self.check = "gender"
            listTableView.reloadData()
        }
        else
        {
            listTableView.isHidden = true
            downArrowOne.image = UIImage(named : "arrow-down-sign-to-navigate.png")
        }
    }
    
    @IBAction func btnMinHt(_ sender: Any)
    {
        if listTableView.isHidden
        {
            listTableView.isHidden = false
            listTableView.frame.origin.x = minHtBtn.frame.origin.x + 8
            listTableView.frame.origin.y = minHtBtn.frame.origin.y + 160
            listTableView.frame.size.height = minHtBtn.frame.size.height + 140
            listTableView.frame.size.width = minHtBtn.frame.size.width
            minHtImg.image = UIImage(named : "up-arrow.png")
            maxHtImg.image = UIImage(named : "arrow-down-sign-to-navigate.png")
            downArrowOne.image = UIImage(named : "arrow-down-sign-to-navigate.png")
            downArrowTwo.image = UIImage(named : "arrow-down-sign-to-navigate.png")
           
            self.check = "HeightOne"
            listTableView.reloadData()
            
        }
        else
        {
            listTableView.isHidden = true
            minHtImg.image = UIImage(named : "arrow-down-sign-to-navigate.png")
        }
    }
    
    @IBAction func btnMaxHt(_ sender: Any)
    {
        if listTableView.isHidden
        {
            listTableView.isHidden = false
            listTableView.frame.origin.x = maxHtBtn.frame.origin.x + 8
            listTableView.frame.origin.y = maxHtBtn.frame.origin.y + 160
            listTableView.frame.size.height = maxHtBtn.frame.size.height + 140
            listTableView.frame.size.width = maxHtBtn.frame.size.width
            listTableView.isHidden = false
            maxHtImg.image = UIImage(named : "up-arrow.png")
            minHtImg.image = UIImage(named : "arrow-down-sign-to-navigate.png")
            downArrowOne.image = UIImage(named : "arrow-down-sign-to-navigate.png")
            downArrowTwo.image = UIImage(named : "arrow-down-sign-to-navigate.png")
            
            self.check = "HeightTwo"
            listTableView.reloadData()
            
        }
        else
        {
            listTableView.isHidden = true
            maxHtImg.image = UIImage(named : "arrow-down-sign-to-navigate.png")
        }
    }
    
    @IBAction func btnCountry(_ sender: Any)
    {
        if listTableView.isHidden
        {
            listTableView.isHidden = false
            listTableView.frame.origin.x = countryView.frame.origin.x + 8
            listTableView.frame.origin.y = countryBtn.frame.origin.y + 204
            listTableView.frame.size.width = countryBtn.frame.size.width
            listTableView.frame.size.height = countryBtn.frame.size.height + 140
            listTableView.isHidden = false
            downArrowTwo.image = UIImage(named : "up-arrow.png")
            minHtImg.image = UIImage(named : "arrow-down-sign-to-navigate.png")
            maxHtImg.image = UIImage(named : "arrow-down-sign-to-navigate.png")
            downArrowOne.image = UIImage(named : "arrow-down-sign-to-navigate.png")
            
            self.check = "Country"
            let params = ""
            self.GetData(urlStr: "countries", params: params, checkStr: "countryStr")
        }
        else
        {
            listTableView.isHidden = true
            downArrowTwo.image = UIImage(named : "arrow-down-sign-to-navigate.png")
            
        }
    }
    
    
    @IBAction func btnSave(_ sender: Any)
    {
      //  user_id:27730895height_to:h6gender:maleage_from:18age_to:50height_from:h1country:118page:0preferred_matches
        
        let params = "user_id=27730895&height_to=\(maxHtBtn.titleLabel?.text!)&gender=\(lookingForBtn.titleLabel?.text!)&age_from=\(ageMinLbl.text!)&age_to=\(ageMaxLbl.text!)&height_from=\(minHtBtn.titleLabel?.text!)&country=\(countryBtn.titleLabel?.text!)&page=0"
        GetData(urlStr: "filters", params: params, checkStr: "saveStr")
    }
    
    
    func GetData(urlStr : String , params : String , checkStr : String){
        // LoadingIndicatorView.show()
        
        ApiResponse.onResponsePostPhp(url: urlStr, parms: params, completion: { (result, error) in
            
            if (error == "") {
                
                let status = result["status"] as! Bool
                let message = result["message"] as! String
                if (status == true) {
                    
                    OperationQueue.main.addOperation {
                        LoadingIndicatorView.hide()
                        
                        if(checkStr == "height")
                        {
                            self.heightArr  = result["data"] as! [NSDictionary]
                            
                            for dict1 in self.heightArr
                            {
                                self.heightList.append(dict1["height_values"] as! String)
                            }
                            
                            self.listTableView.reloadData()
                        }
                        else if (checkStr == "countryStr")
                        {
                            self.countryArr = result["data"] as! [NSDictionary]
                            for dictCountry in self.countryArr
                            {
                                self.countryList.append(dictCountry["name"] as! String)
                            }
                            
                            self.listTableView.reloadData()
                        }
                        else
                        {
                            let saveDict = result["data"] as! NSDictionary
                            ApiResponse.alert(title: "", message: message, controller: self)
                        }
                        
                    }
                }
                else
                {
                    ApiResponse.alert(title: "Ooooops", message: message, controller: self)
                }
            }
            else
            {
                if (error == Constant.Status_Not_200)
                {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else
                {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        })
    }
    
    
}

extension RefineViewController : UITableViewDataSource , UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
       if (check == "HeightOne") || (check == "HeightTwo")
       {
        return heightArr.count
       }
        else if (check == "Country")
        {
            return countryArr.count
        }
        else
        {
            return genderList.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
       
        if (check == "HeightOne") || (check == "HeightTwo")
        {
             let cell : ListCell = listTableView.dequeueReusableCell(withIdentifier: "listcell") as! ListCell
            cell.lnameLbl.text! = heightList[indexPath.row]
          //  cell.lnameLbl.text! = heightDict["h1"] as! String
            print("height == \(cell.lnameLbl.text!)")
            
            return cell
        }
        else if (check == "Country")
        {
             let cell : ListCell = listTableView.dequeueReusableCell(withIdentifier: "listcell") as! ListCell
             cell.lnameLbl.text! = countryList[indexPath.row]
            return cell
        }
      else
        {
             let cell : ListCell = listTableView.dequeueReusableCell(withIdentifier: "listcell") as! ListCell
             cell.lnameLbl.text! = genderList[indexPath.row]
             print("gender = \(genderList[indexPath.row])")
             return cell
        }
        
        
      
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        listTableView.isHidden = true
        minHtImg.image = UIImage(named : "arrow-down-sign-to-navigate.png")
        maxHtImg.image = UIImage(named : "arrow-down-sign-to-navigate.png")
        downArrowOne.image = UIImage(named : "arrow-down-sign-to-navigate.png")
        downArrowTwo.image = UIImage(named : "arrow-down-sign-to-navigate.png")
        
        if(check == "HeightOne")
        {
     
            minHtBtn.setTitle(heightList[indexPath.row], for: .normal)
        }
        else if(check == "HeightTwo")
        {
            
            maxHtBtn.setTitle(heightList[indexPath.row], for: .normal)
        }
        else if (check == "Country")
        {
            countryBtn.setTitle(countryList[indexPath.row], for: .normal)
        }
        else
        {
            lookingForBtn.setTitle(genderList[indexPath.row], for: .normal)
        }
        
    }
    
}

class ListCell : UITableViewCell
{
    @IBOutlet weak var lnameLbl: UILabel!
}
