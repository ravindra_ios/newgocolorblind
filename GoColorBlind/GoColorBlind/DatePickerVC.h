//
//  DatePickerVC.h
//  Kruze
//
//  Created by Developer on 20/07/16.
//  Copyright © 2016 apra.gj@gmail.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"

@protocol DatePickerVCProtocol <NSObject>

-(void)set_SelectedCalenderDate:(NSString *)sender; //I am thinking my data is NSArray, you can use another object for store your information.
-(void)did_SelectCalenderDate:(NSDate *)sender;

@end

@interface DatePickerVC : UIViewController {
    
    NSString *currentDate;
    NSString *selectDate;
    IBOutlet UIDatePicker *dataPicker;
}

@property(nonatomic,assign) id delegate;
@property(nonatomic,assign) NSInteger isComeFrom;
@property(nonatomic,assign) NSString *dateFormatte;
@property(nonatomic,assign) NSInteger datePickerMode;


@property (nonatomic, strong) IBOutlet UIButton *btn_Calender;
@property (nonatomic, strong) IBOutlet UILabel *lbl_CalenderHeader;

@end
