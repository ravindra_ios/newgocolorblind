//
//  DatePickerVC.m
//  Kruze
//
//  Created by Developer on 20/07/16.
//  Copyright © 2016 apra.gj@gmail.com. All rights reserved.
//

#import "DatePickerVC.h"

@interface DatePickerVC ()

@end

@implementation DatePickerVC




- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    NSString *nibName = nil;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        if ([[UIScreen mainScreen] bounds].size.height==568)
        {
            nibName=@"DatePickerVC";
        }
        else if ([[UIScreen mainScreen] bounds].size.height==480)
        {
            nibName=@"DatePickerVC";
        }
        else if ([[UIScreen mainScreen] bounds].size.height==667)
        {
            nibName=@"DatePickerVC";
        }
        else if ([[UIScreen mainScreen] bounds].size.height==736)
        {
            nibName=@"DatePickerVC";
        }
    }
//    else
//    {
//        nibName=@"DatePickerVC_iPad";
//    }
//    
    self = [super initWithNibName:nibName bundle:nibBundleOrNil];
    
    if (self)
    {
        
    }
    
    return self;
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    _dateFormatte = @"YYYY-MM-dd";
    
    // Do any additional setup after loading the view from its nib.
    [_btn_Calender setTintColor:[UIColor whiteColor]];
    
    if(_datePickerMode == 0){
        [dataPicker setDatePickerMode:UIDatePickerModeDate];
    }else{
        [dataPicker setDatePickerMode:UIDatePickerModeDateAndTime];
 
    }
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    
   
    [formatter setDateFormat:_dateFormatte];
    [dataPicker setDate:[NSDate date]];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDate *currentDat = [NSDate date];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setYear:0];
   // NSDate *maxDate = [calendar dateByAddingComponents:comps toDate:currentDat options:0];
   // [comps setYear:0];
    
    //if (_isComeFrom == 2) {
        
      //  [dataPicker setMinimumDate:maxDate];

   // }
   // else{
        
     //   [dataPicker setMaximumDate:maxDate];

    //}

    [formatter setDateFormat:_dateFormatte];
    currentDate = [formatter stringFromDate:[NSDate date]];
    
    [formatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [formatter setDateFormat:@"EEEE"];
    NSString *weekDay =  [formatter stringFromDate:[NSDate date]];
    
    NSString *completeDate = [NSString stringWithFormat:@"%@, %@",weekDay,currentDate];
    
    _lbl_CalenderHeader.text = completeDate;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated
{
  //  [_delegate set_SelectedCalenderDate:selectDate];
}


- (IBAction)datePicker_ValueChange:(id)sender{

    [self selected_Date];
}


-(void)selected_Date{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
   // YYYY-MM-dd
    [formatter setDateFormat:@"YYYY-MM-dd"];
    
    selectDate = [formatter stringFromDate:dataPicker.date];
    
    _lbl_CalenderHeader.text =[NSString stringWithFormat:@"%@", selectDate];

}

#pragma mark - IBAction Cancel / Done
#pragma mark -

- (IBAction)action_Cancel:(id)sender{
    
    [self dismissViewControllerAnimated:NO completion:nil];
}


- (IBAction)action_Done:(id)sender{
    
    NSDateFormatter *format=[[NSDateFormatter alloc] init];
    [format setDateFormat:_dateFormatte];
    currentDate = [format stringFromDate:dataPicker.date];
    selectDate =[format stringFromDate:dataPicker.date];
    [dataPicker reloadInputViews];
    
   // [_delegate did_SelectCalenderDate:dataPicker.date];
    [_delegate set_SelectedCalenderDate:selectDate];

    [self dismissViewControllerAnimated:NO completion:^{
        
        //[SearchRouteVC  action_DatePickerDone:selectDate];
    }];

}


- (IBAction)action_Close:(id)sender{
    
    [self dismissViewControllerAnimated:NO completion:^{
        
    }];
}


#pragma mark - @end
#pragma mark -

@end
