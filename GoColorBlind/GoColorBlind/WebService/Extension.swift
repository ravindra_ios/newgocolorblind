//
//  Extension.swift
//  Shared
//
//  Created by admin on 5/6/17.
//  Copyright © 2017 admin. All rights reserved.
//

import Foundation
import UIKit

public extension UIColor {
    
  class func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
 
 
    
    
    class func getSearchBGColor() -> UIColor{
        return UIColor(red:245.0/255.0, green:245.0/255.0 ,blue:245.0/255.0 , alpha:1.00)
    }
    class func getCustomBlueColor() -> UIColor{
        return UIColor(red:17.0/255.0, green:67.0/255.0 ,blue:100.0/255.0 , alpha:1.00)
    }
    
    class func getButtonBlueColor() -> UIColor{
        return UIColor(red:49.0/255.0, green:155.0/255.0 ,blue:245.0/255.0 , alpha:1.00)
    }
    
    class func getCellBGColor() -> UIColor{
        return UIColor(red:223.0/255.0, green:223.0/255.0 ,blue:223.0/255.0 , alpha:1.00)
    }
    class func getLinkBlueColor() -> UIColor{
        return UIColor(red:49.0/255.0, green:155.0/255.0 ,blue:245.0/255.0 , alpha:1.00)
    }
    
}

extension String {
    func containsAlphabets() -> Bool {
        //Checks if all the characters inside the string are alphabets
        let set = CharacterSet.letters
        return self.utf16.contains( where: {
            guard let unicode = UnicodeScalar($0) else { return false }
            return set.contains(unicode)
        })
    }
}
extension CALayer {
    
    func addShadow() {
        
        let shadow = CALayer()
        shadow.shadowOffset = CGSize(width: 2, height:5);
        shadow.cornerRadius = 4;
        shadow.masksToBounds = false;
        shadow.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        shadow.shadowRadius = 1.0
        self.addSublayer(shadow)
    }
    
    func shadow_Show()  {
        self.shadowOpacity = 0.4
    }
    
    func shadow_Hide()  {
        self.shadowOpacity = 0.0;
    }
    
    //-----------Border--------------------
    func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat) {
        
        let border = CALayer()
        
        switch edge {
        case UIRectEdge.top:
            border.frame = CGRect.init(x: 0, y: 0, width: frame.width, height: thickness)
            break
        case UIRectEdge.bottom:
            border.frame = CGRect.init(x: 0, y: frame.height - thickness, width: frame.width, height: thickness)
            break
        case UIRectEdge.left:
            border.frame = CGRect.init(x: 0, y: 0, width: thickness, height: frame.height)
            break
        case UIRectEdge.right:
            border.frame = CGRect.init(x: frame.width - thickness, y: 0, width: thickness, height: frame.height)
            break
        default:
            break
        }
        
        border.backgroundColor = color.cgColor;
        
        self.addSublayer(border)
    }
}
