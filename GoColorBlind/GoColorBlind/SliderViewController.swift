//
//  SliderViewController.swift
//  GoColorBlind
//
//  Created by Ravi on 11/22/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit


class SliderViewController: UIViewController {

   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
//        rangeSlider1.trackHighlightTintColor = UIColor.init(red: 253.0/255, green: 89.0/255, blue: 95.0/255, alpha: 1.0)
//        rangeSlider2.trackHighlightTintColor = UIColor.init(red: 253.0/255, green: 89.0/255, blue: 95.0/255, alpha: 1.0)
//        rangeSlider2.curvaceousness = 4.0
//
//        view.addSubview(rangeSlider1)
//        view.addSubview(rangeSlider2)
//
//        rangeSlider1.addTarget(self, action: #selector(SliderViewController.rangeSliderValueChanged(_:)), for: .valueChanged)
    }

    // 253 89 95
    
//    override func viewDidLayoutSubviews() {
//        let margin: CGFloat = 20.0
//        let width = view.bounds.width - 2.0 * margin
//        rangeSlider1.frame = CGRect(x: margin, y: margin + topLayoutGuide.length + 50,
//                                    width: width, height: 20.0)
//        rangeSlider2.frame = CGRect(x: margin , y: 5 * margin + topLayoutGuide.length + 25,
//                                    width: width , height: 20.0)
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func rangeSliderValueChanged(_ rangeSlider: RangeSlider) {
        print("Range slider value changed: (\(rangeSlider.lowerValue) , \(rangeSlider.upperValue))")
    }

}
