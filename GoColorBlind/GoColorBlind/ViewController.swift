//
//  ViewController.swift
//  GoColorBlind
//
//  Created by Prashant Shinde on 11/10/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit

class ViewController: UIViewController , UITextFieldDelegate{

    //***********************
    //MARK:- Properties
    //***********************
    @IBOutlet var btnRegister: UIButton!
    @IBOutlet var btnSignIn: UIButton!
    @IBOutlet var viewText: UIView!
    
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtPassword: UITextField!
    
    var reach = Reachability()
    var orders : [NSDictionary] = []
    
    //***********************
    //MARK:- Application Life Cycle
    //***********************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SettingView()
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    func SettingView() {
        viewText.layer.cornerRadius = 3
        btnSignIn.layer.cornerRadius = 5
        btnRegister.layer.cornerRadius = 5
        
        btnRegister.layer.borderWidth = 1
        btnRegister.layer.borderColor = UIColor.white.cgColor
        
        let textf : [UITextField] = [txtEmail,txtPassword]
        var plachold = ["Email Id","Password"]
        var i = 0
        
        for tf in textf{
            
            tf.delegate = self
            let paddingview = UIView(frame: CGRect(x:0,y:0,width:10,height:self.view.frame.height))
            tf.leftView = paddingview
            tf.leftViewMode = UITextFieldViewMode .always
            
            tf.attributedPlaceholder = NSAttributedString(string:plachold[i],
                                                          attributes:[NSForegroundColorAttributeName: UIColor.darkGray])
            tf.layer.masksToBounds = false
            tf.layer.cornerRadius = tf.frame.size.width/20
            tf.clipsToBounds = true
            //tf.layer.borderWidth = 1.0
            i += 1

            //tf.layer.borderColor = UIColor.init(red: 35/255.0, green: 118/255.0, blue: 188/255.0, alpha: 1.0).cgColor
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //***********************
    //MARK:- Action button method
    //***********************
    
    @IBAction func actionLogin(_ sender: Any){
        
    }
    
    @IBAction func actionRegister(_ sender: Any){
        
    }
    
    @IBAction func actionForget(_ sender: Any){
        
    }
    
    //***********************
    //MARK:-
    //***********************
    
    func checkbylocation_Api()
    {
        LoadingIndicatorView.show()
        
        let params = "id=72&lat=22.753284800000003&long=75.8936962&page=0"
        print("lll params == \(params)")
        
        ApiResponse.onResponsePostPhp(url: "checkbylocation", parms: params, completion: { (result, error) in
            print("lll response == \(result)")
            
            if (error == "") {
                let status = result["status"] as! Bool
                let message = result["message"] as! String
                if (status == true)
                {
                    OperationQueue.main.addOperation {
                        LoadingIndicatorView.hide()
                        self.orders = result["data"] as! [NSDictionary]
                        print("self.orders : \(self.orders)")
                    }
                }
                else {
                    ApiResponse.alert(title: "Ooooops", message: message, controller: self)
                }
            }
            else {
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        })
    }
    

}

