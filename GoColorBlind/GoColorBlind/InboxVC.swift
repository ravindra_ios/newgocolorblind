//
//  InboxVC.swift
//  GoColorBlind
//
//  Created by Ravi on 11/20/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit

class InboxVC: UIViewController {

    @IBOutlet weak var inboxCollectionView: UICollectionView!
    @IBOutlet weak var invitationTableView: UITableView!
    @IBOutlet weak var viewOne: UIView!
    @IBOutlet weak var viewTwo: UIView!
    @IBOutlet weak var viewThree: UIView!
    @IBOutlet weak var viewFour: UIView!
    @IBOutlet weak var acceptTableView: UITableView!
    @IBOutlet weak var sendTableView: UITableView!
    @IBOutlet weak var deleteTableView: UITableView!
    
    var inboxArr : [String] = []
    var invitationArr : [NSDictionary] = []
    var acceptArr : [NSDictionary] = []
    var sendArr : [NSDictionary] = []
    var deleteArr : [NSDictionary] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        inboxArr = ["Invitations" , "Accepted" , "Sent Items" , "Deleted"]
        self.invitationData()
        
        viewOne.isHidden = false
        viewTwo.isHidden = true
        viewThree.isHidden = true
        viewFour.isHidden = true
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
   func invitationData()
   {
    LoadingIndicatorView.show("Loading...")
    let params = "user_id=24386013"
    
    ApiResponse.onResponsePostPhp(url: "show_invitations", parms: params, completion: { (result, error) in
    
    if (error == "")
    {
    let status = result["status"] as! Bool
    let message = result["message"] as! String
    print("result == \(result)")
    if (status == true)
    {
    OperationQueue.main.addOperation
    {
    LoadingIndicatorView.hide()
    self.invitationArr = result["data"] as! [NSDictionary]
        self.invitationTableView.reloadData()
    }
    }
    else
    {
    ApiResponse.alert(title: "", message: message, controller: self)
    }
    }
    else {
    if (error == Constant.Status_Not_200) {
    ApiResponse.alert(title: "Something went wrong", message: "Please try again!", controller: self)
    }
    else
    {
    ApiResponse.alert(title: "Request Time Out", message: "Please check your internet", controller: self)
    }
    }
    })
    }
}

extension InboxVC : UICollectionViewDataSource , UICollectionViewDelegate
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return inboxArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : InboxCell = inboxCollectionView.dequeueReusableCell(withReuseIdentifier: "inboxcell", for: indexPath) as! InboxCell
        cell.inboxLabel.text! = inboxArr[indexPath.item]
        
        if(indexPath.item == 0)
        {
        }
        else if(indexPath.item == 1){}
        else if(indexPath.item == 2){}
        else {}
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell : InboxCell = inboxCollectionView.dequeueReusableCell(withReuseIdentifier: "inboxcell", for: indexPath) as! InboxCell
        cell.inboxLabel.text! = inboxArr[indexPath.item]
        
        if(indexPath.item == 0)
        {
            print("a")
            viewOne.isHidden = false
            viewTwo.isHidden = true
            viewThree.isHidden = true
            viewFour.isHidden = true
        }
        else if(indexPath.item == 1)
        {
             print("b")
            viewOne.isHidden = true
            viewTwo.isHidden = false
            viewThree.isHidden = true
            viewFour.isHidden = true
        }
        else if(indexPath.item == 2)
        {
             print("c")
            viewOne.isHidden = true
            viewTwo.isHidden = true
            viewThree.isHidden = false
            viewFour.isHidden = true
        }
        else
        {
             print("d")
            viewOne.isHidden = true
            viewTwo.isHidden = true
            viewThree.isHidden = true
            viewFour.isHidden = false
        }
        
        }
}

extension InboxVC : UITableViewDataSource , UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == invitationTableView
        {
          return invitationArr.count
        }
        else if tableView == acceptTableView
        {
            return acceptArr.count
        }
        else if tableView == sendTableView
        {
            return sendArr.count
        }
        else
        {
            return deleteArr.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(tableView == invitationTableView)
        {
            let cell : InvitationCell = invitationTableView.dequeueReusableCell(withIdentifier: "invitationcell") as! InvitationCell
            
            let dict = invitationArr[indexPath.row]
            let s1 = dict["age"] as! String
            let s2 = dict["heights"] as! String
            let s3 = dict["language"] as! String
            cell.iNameLbl.text! = dict["first_name"] as! String
            cell.iAge.text! = "\(s1), \(s2)\(s3)"
            cell.iOccupationLbl.text! = dict["occupations"] as! String
            cell.iAddressLbl.text! = dict["address"] as! String
            print("s3 == \(s3)")
            
            cell.calcelBtn.layer.borderWidth = 1.2
            cell.calcelBtn.layer.borderColor = UIColor.init(red: 110/255, green: 110/255, blue: 110/255, alpha: 1.0).cgColor
            cell.calcelBtn.layer.cornerRadius = 4.0
            
            cell.addBtn.layer.borderWidth = 1.2
            cell.addBtn.layer.borderColor = UIColor.init(red: 110/255, green: 110/255, blue: 110/255, alpha: 1.0).cgColor
            cell.addBtn.layer.cornerRadius = 4.0
            
            cell.IImage.layer.masksToBounds = false
            cell.IImage.layer.cornerRadius = cell.IImage.frame.size.width/2
            cell.IImage.clipsToBounds = true
            cell.IImage.layer.borderWidth = 1.0
            cell.IImage.layer.borderColor = UIColor.init(red: 110/255, green: 110/255, blue: 110/255, alpha: 1.0).cgColor
            
            var imgStr = dict["main_profile_image"] as! String
            imgStr = imgStr.replacingOccurrences(of: " ", with: "%20")
            let imgUrl = ImgBase_Url + imgStr
            cell.IImage.sd_setImage(with: URL(string : "\(imgUrl)")! as URL)
            
            return cell
        }
        else if tableView == acceptTableView
        {
            let cell : AcceptCell = acceptTableView.dequeueReusableCell(withIdentifier: "acceptcell") as! AcceptCell
            
            let dict = acceptArr[indexPath.row]
            let s1 = dict["age"] as! String
            let s2 = dict["heights"] as! String
            let s3 = dict["language"] as! String
            cell.aNameLbl.text! = dict["first_name"] as! String
            cell.aAgeLbl.text! = "\(s1), \(s2)\(s3)"
            cell.aOccupationLbl.text! = dict["occupations"] as! String
            cell.aAddressLbl.text! = dict["address"] as! String
            print("s3 == \(s3)")
            
            
            
            cell.aImage.layer.masksToBounds = false
            cell.aImage.layer.cornerRadius = cell.aImage.frame.size.width/2
            cell.aImage.clipsToBounds = true
            cell.aImage.layer.borderWidth = 1.0
            cell.aImage.layer.borderColor = UIColor.init(red: 110/255, green: 110/255, blue: 110/255, alpha: 1.0).cgColor
            
            var imgStr = dict["main_profile_image"] as! String
            imgStr = imgStr.replacingOccurrences(of: " ", with: "%20")
            let imgUrl = ImgBase_Url + imgStr
            cell.aImage.sd_setImage(with: URL(string : "\(imgUrl)")! as URL)
            
            return cell
        }
        else if tableView == sendTableView
        {
            let cell : SendCell = acceptTableView.dequeueReusableCell(withIdentifier: "sendcell") as! SendCell
            
            let dict = sendArr[indexPath.row]
            let s1 = dict["age"] as! String
            let s2 = dict["heights"] as! String
            let s3 = dict["language"] as! String
            cell.sNameLbl.text! = dict["first_name"] as! String
            cell.sAgeLbl.text! = "\(s1), \(s2)\(s3)"
            cell.sOccupationLbl.text! = dict["occupations"] as! String
            cell.sAddressLbl.text! = dict["address"] as! String
            print("s3 == \(s3)")
            
            
            
            cell.sImage.layer.masksToBounds = false
            cell.sImage.layer.cornerRadius = cell.sImage.frame.size.width/2
            cell.sImage.clipsToBounds = true
            cell.sImage.layer.borderWidth = 1.0
            cell.sImage.layer.borderColor = UIColor.init(red: 110/255, green: 110/255, blue: 110/255, alpha: 1.0).cgColor
            
            var imgStr = dict["main_profile_image"] as! String
            imgStr = imgStr.replacingOccurrences(of: " ", with: "%20")
            let imgUrl = ImgBase_Url + imgStr
            cell.sImage.sd_setImage(with: URL(string : "\(imgUrl)")! as URL)
            
            return cell
        }
        else
        {
            let cell : DeleteCell = acceptTableView.dequeueReusableCell(withIdentifier: "deletecell") as! DeleteCell
            
            let dict = deleteArr[indexPath.row]
            let s1 = dict["age"] as! String
            let s2 = dict["heights"] as! String
            let s3 = dict["language"] as! String
            cell.dName.text! = dict["first_name"] as! String
            cell.dAge.text! = "\(s1), \(s2)\(s3)"
            cell.dOccupation.text! = dict["occupations"] as! String
            cell.dAddress.text! = dict["address"] as! String
            print("s3 == \(s3)")
            
            
            
            cell.dImage.layer.masksToBounds = false
            cell.dImage.layer.cornerRadius = cell.dImage.frame.size.width/2
            cell.dImage.clipsToBounds = true
            cell.dImage.layer.borderWidth = 1.0
            cell.dImage.layer.borderColor = UIColor.init(red: 110/255, green: 110/255, blue: 110/255, alpha: 1.0).cgColor
            
            var imgStr = dict["main_profile_image"] as! String
            imgStr = imgStr.replacingOccurrences(of: " ", with: "%20")
            let imgUrl = ImgBase_Url + imgStr
            cell.dImage.sd_setImage(with: URL(string : "\(imgUrl)")! as URL)
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == invitationTableView
        {
            return 149
        }
        else
        {
            return 105
        }
        
    }
    
    func requestIgnore(_ sender : UIButton)
    {
        let point : CGPoint = sender.convert(CGPoint.zero , to : invitationTableView)
        let indexPath1 = invitationTableView.indexPathForRow(at: point)
        let dict = invitationArr[(indexPath1?.row)!]
        let req_cancel_id = dict["id"] as! String
        self.cancelRequest()
    }
    
    func requestAdd(_ sender : UIButton)
    {
        let point : CGPoint = sender.convert(CGPoint.zero, to: invitationTableView)
        let indexPath1 = invitationTableView.indexPathForRow(at: point)
        let dict1 = invitationArr[(indexPath1?.row)!]
        let ignore_id = dict1["id"] as! String
    }
    
    func cancelRequest()
    {
        LoadingIndicatorView.show("Loading...")
        let params = "user_id=27730811&matches_id:24386013"
        
        ApiResponse.onResponsePostPhp(url: "cancel_friend_request", parms: params, completion: { (result, error) in
            
            if (error == "")
            {
                let status = result["status"] as! Bool
                let message = result["message"] as! String
                print("result == \(result)")
                if (status == true)
                {
                    OperationQueue.main.addOperation
                        {
                            LoadingIndicatorView.hide()
                            self.invitationArr = result["data"] as! [NSDictionary]
                            self.invitationTableView.reloadData()
                    }
                }
                else
                {
                    ApiResponse.alert(title: "", message: message, controller: self)
                }
            }
            else
            {
                if (error == Constant.Status_Not_200)
                {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again!", controller: self)
                }
                else
                {
                    ApiResponse.alert(title: "Request Time Out", message: "Please check your internet", controller: self)
                }
            }
        })
    }
    
    
    func addRequest()
    {
        LoadingIndicatorView.show("Loading...")
        let params = "user_id=27730811&matches_id:24386013"
        
        ApiResponse.onResponsePostPhp(url: "confirm_friend_request", parms: params, completion: { (result, error) in
            
            if (error == "")
            {
                let status = result["status"] as! Bool
                let message = result["message"] as! String
                print("result == \(result)")
                if (status == true)
                {
                    OperationQueue.main.addOperation
                        {
                            LoadingIndicatorView.hide()
                            self.invitationArr = result["data"] as! [NSDictionary]
                            self.invitationTableView.reloadData()
                    }
                }
                else
                {
                    ApiResponse.alert(title: "", message: message, controller: self)
                }
            }
            else {
                if (error == Constant.Status_Not_200)
                {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again!", controller: self)
                }
                else
                {
                    ApiResponse.alert(title: "Request Time Out", message: "Please check your internet", controller: self)
                }
            }
        })
    }
    
    
}

class InboxCell : UICollectionViewCell
{
    @IBOutlet weak var inboxLabel: UILabel!
    
}

class InvitationCell : UITableViewCell
{
    @IBOutlet weak var iNameLbl: UILabel!
    @IBOutlet weak var iAge: UILabel!
    @IBOutlet weak var iOccupationLbl: UILabel!
    @IBOutlet weak var iAddressLbl: UILabel!
    @IBOutlet weak var IImage: UIImageView!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var calcelBtn: UIButton!
    
}

class AcceptCell : UITableViewCell
{
    @IBOutlet weak var aNameLbl: UILabel!
    @IBOutlet weak var aAgeLbl: UILabel!
    @IBOutlet weak var aOccupationLbl: UILabel!
    @IBOutlet weak var aAddressLbl: UILabel!
    @IBOutlet weak var aImage: UIImageView!
    
}

class SendCell : UITableViewCell
{
    @IBOutlet weak var sImage: UIImageView!
    @IBOutlet weak var sNameLbl: UILabel!
    @IBOutlet weak var sAgeLbl: UILabel!
    @IBOutlet weak var sOccupationLbl: UILabel!
    @IBOutlet weak var sAddressLbl: UILabel!
    
}

class DeleteCell : UITableViewCell
{
    @IBOutlet weak var dImage: UIImageView!
    @IBOutlet weak var dName: UILabel!
    @IBOutlet weak var dAge: UILabel!
    @IBOutlet weak var dOccupation: UILabel!
    @IBOutlet weak var dAddress: UILabel!
    
}
