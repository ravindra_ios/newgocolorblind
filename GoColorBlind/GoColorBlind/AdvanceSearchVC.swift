//
//  AdvanceSearchVC.swift
//  GoColorBlind
//
//  Created by Ravi on 11/23/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit

class AdvanceSearchVC: UIViewController {

    @IBOutlet weak var minAdvanceLbl: UILabel!
    @IBOutlet weak var maxAdvanceLbl: UILabel!
    @IBOutlet weak var viewOne: UIView!
    @IBOutlet weak var viewTwo: UIView!
    @IBOutlet weak var viewThree: UIView!
    @IBOutlet weak var viewFour: UIView!
    @IBOutlet weak var viewFive: UIView!
    @IBOutlet weak var viewSix: UIView!
    @IBOutlet weak var viewSeven: UIView!
    @IBOutlet weak var viewEight: UIView!
    @IBOutlet weak var viwNine: UIView!
    
    @IBOutlet weak var moreBtnOne: UIButton!
    @IBOutlet weak var moreBtnTwo: UIButton!
    @IBOutlet weak var moreBtnThree: UIButton!
    @IBOutlet weak var moreBtnFour: UIButton!
    @IBOutlet weak var moreBtnFive: UIButton!
    @IBOutlet weak var moreBtnSix: UIButton!
    @IBOutlet weak var moreBtnSeven: UIButton!
    @IBOutlet weak var moreBtnEight: UIButton!
    @IBOutlet weak var mainViewOne: UIView!
    @IBOutlet weak var listTableView: UITableView!
    @IBOutlet weak var listTableViewOne: UITableView!
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var heightToBtn: UIButton!
    @IBOutlet weak var heightFrom: UIButton!
    @IBOutlet weak var countryBtn: UIButton!
    @IBOutlet weak var stateBtn: UIButton!
    @IBOutlet weak var cityBtn: UIButton!
    @IBOutlet weak var genderBtn: UIButton!
    @IBOutlet weak var hairColorBtn: UIButton!
    @IBOutlet weak var hairLengthBtn: UIButton!
    @IBOutlet weak var hairTypeBtn: UIButton!
    @IBOutlet weak var eyeColorBtn: UIButton!
    
    @IBOutlet weak var eyeWearBtn: UIButton!
    @IBOutlet weak var bodyArtBtn: UIButton!
    @IBOutlet weak var bodyTypeBtn: UIButton!
    @IBOutlet weak var ethnicityBtn: UIButton!
    @IBOutlet weak var facialHairBtn: UIButton!
    
    @IBOutlet weak var featureBtn: UIButton!
    @IBOutlet weak var appereanceBtn: UIButton!
    @IBOutlet weak var drinkBtn: UIButton!
    @IBOutlet weak var smokeBtn: UIButton!
    @IBOutlet weak var maritalBtn: UIButton!
    
    @IBOutlet weak var childrenBtn: UIButton!
    @IBOutlet weak var noOfChildBtn: UIButton!  // hide
    @IBOutlet weak var oldChildBtn: UIButton!   // hide
    @IBOutlet weak var youngChildBtn: UIButton!  // hide
    @IBOutlet weak var moreChildBtn: UIButton!   // hide
    
    @IBOutlet weak var petsBtn: UIButton!
    @IBOutlet weak var occupationBtn: UIButton!
    @IBOutlet weak var employmentStatusBtn: UIButton!
    @IBOutlet weak var annualIncomeBtn: UIButton!
    @IBOutlet weak var homeTypeBtn: UIButton!
    
    
    @IBOutlet weak var situationBtn: UIButton!
    @IBOutlet weak var realocateBtn: UIButton!
    @IBOutlet weak var relationshipBtn: UIButton!
    @IBOutlet weak var nationalityBtn: UIButton!
    @IBOutlet weak var educationBtn: UIButton!
    
    @IBOutlet weak var langSpokenBtn: UIButton!
    @IBOutlet weak var langAbilityBtn: UIButton!
    @IBOutlet weak var religionBtn: UIButton!
    @IBOutlet weak var starSignBtn: UIButton!
    @IBOutlet weak var funBtn: UIButton!
    
    @IBOutlet weak var foodBtn: UIButton!
    @IBOutlet weak var musicBtn: UIButton!
    @IBOutlet weak var playBtn: UIButton!
    
    
    
    var checkOne : String = ""
    var heighrToArr : [NSDictionary] = []
    var heightToList : [String] = []
    var countryArrOne : [NSDictionary] = []
    var countryList : [String] = []
    var country_id : String = ""
    var stateArr : [NSDictionary] = []
    var stateList : [String] = []
    var state_id : String = ""
    var cityArr : [NSDictionary] = []
    var cityList : [String] = []
    var genderListOne : [String] = []
    var hairColorArr : [NSDictionary] = []
    var hairColorLis : [String] = []
    var hairLengthArr : [NSDictionary] = []
    var hairLengthList : [String] = []
    var hairTypeArr : [NSDictionary] = []
    var hairTypeList : [String] = []
    var eyeColorArr : [NSDictionary] = []
    var eyeColorList : [String] = []
    
    var eyeWearArr : [NSDictionary] = []
    var eyeWearList : [String] = []
    var bodyArtArr : [NSDictionary] = []
    var bodyArtList : [String] = []
    var bodyTypeArr : [NSDictionary] = []
    var bodyTypeList : [String] = []
    var ethnicityArr : [NSDictionary] = []
    var ethnicityList : [String] = []
    var facialArr : [NSDictionary] = []
    var facialList : [String] = []
    
    var featureArr : [NSDictionary] = []
    var featureList : [String] = []
    var appereanceArr : [NSDictionary] = []
    var appereanceList : [String] = []
    var drinkArr : [NSDictionary] = []
    var drinkList : [String] = []
    var smokeArr : [NSDictionary] = []
    var smokeList : [String] = []
    var maritalArr : [NSDictionary] = []
    var maritalList : [String] = []
    
    var childrenArr : [NSDictionary] = []
    var childrenList : [String] = []
   
    var petsArr : [NSDictionary] = []
    var petsList : [String] = []
    var occupationArr : [NSDictionary] = []
    var occupationList : [String] = []
    var employmentStatusArr : [NSDictionary] = []
    var employmentStatusList : [String] = []
    var annualIncomeArr : [NSDictionary] = []
    var annualIncomeList : [String] = []
    var homeTypeArr : [NSDictionary] = []
    var homeTYpeList : [String] = []
    
    var situationArr : [NSDictionary] = []
    var situationList : [String] = []
    var realocateArr : [NSDictionary] = []
    var realocateList : [String] = []
    var relationShipArr : [NSDictionary] = []
    var relationshipList : [String] = []
    var nationalityArr : [NSDictionary] = []
    var nationalityList : [String] = []
    var educationArr : [NSDictionary] = []
    var educationList : [String] = []
    
    var langSpokenArr : [NSDictionary] = []
    var langSpokenList : [String] = []
    var langAbilityArr : [NSDictionary] = []
    var langAbilityList : [String] = []
    var religionArr : [NSDictionary] = []
    var religionList : [String] = []
    var starSignArr : [NSDictionary] = []
    var starSignList : [String] = []
    var funArr : [NSDictionary] = []
    var funList : [String] = []
    
    var foodArr : [NSDictionary] = []
    var foodList : [String] = []
    var musicArr : [NSDictionary] = []
    var musicList : [String] = []
    var playArr : [NSDictionary] = []
    var playList : [String] = []
    
    let rangeSlider1 = RangeSlider(frame: CGRect.zero)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
//        http://featuremii.net/old_data/api/advance_searching_view
//        ab check karna
//        http://featuremii.net/old_data/api/countries
//        http://featuremii.net/old_data/api/state
//        id
//        http://featuremii.net/old_data/api/city

        listTableViewOne.layer.borderColor = UIColor.init(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.2).cgColor
        listTableViewOne.layer.borderWidth = 1.3
        listTableViewOne.frame.size.height = 325
        
        listTableViewOne.isHidden = true
        
        viewTwo.isHidden = true
        viewThree.isHidden = true
        viewFour.isHidden = true
        viewFive.isHidden = true
        viewSix.isHidden = true
        viewSeven.isHidden = true
        viewEight.isHidden = true
        viwNine.isHidden = true
        
        moreBtnTwo.isHidden = true
        moreBtnThree.isHidden = true
        moreBtnFour.isHidden = true
        moreBtnFive.isHidden = true
        moreBtnSix.isHidden = true
        moreBtnSeven.isHidden = true
        moreBtnEight.isHidden = true
        
        rangeSlider1.trackHighlightTintColor = UIColor.init(red: 253.0/255, green: 89.0/255, blue: 95.0/255, alpha: 1.0)
        view.addSubview(rangeSlider1)
        rangeSlider1.addTarget(self, action: #selector(SliderViewController.rangeSliderValueChanged(_:)), for: .valueChanged)
        
        self.genderListOne = ["None" , "Male" , "Female"]
        self.GetData(urlStr: "advance_searching_view", params: "", checkStr: "height")
        
    }
    
    override func viewDidLayoutSubviews()
    {
        let margin: CGFloat = 20.0
        let width = view.bounds.width - 2.0 * margin
        rangeSlider1.frame = CGRect(x: margin, y: margin + topLayoutGuide.length + 60,
                                    width: width, height: 28.0)
    
        heightToBtn.titleEdgeInsets.left = 5
        heightFrom.titleEdgeInsets.left = 5
        countryBtn.titleEdgeInsets.left = 5
        stateBtn.titleEdgeInsets.left = 5
        cityBtn.titleEdgeInsets.left = 5
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func rangeSliderValueChanged(_ rangeSlider: RangeSlider)
    {
        print("Range slider value changed: (\(rangeSlider.lowerValue) , \(rangeSlider.upperValue))")
        let minValue = Int(rangeSlider.lowerValue)
        minAdvanceLbl.text! = "\(minValue)"
        let maxValue = Int(rangeSlider.upperValue)
        maxAdvanceLbl.text! = "\(maxValue)"
    }
    
    @IBAction func m1Btn(_ sender: Any)
    {
        viewTwo.isHidden = false
        moreBtnTwo.isHidden = false
        moreBtnOne.isHidden = true
        viewTwo.frame.origin.x = viewOne.frame.origin.x
        viewTwo.frame.origin.y = moreBtnOne.frame.origin.y - 5
        moreBtnTwo.frame.origin.y = viewTwo.frame.origin.y + 425
    }
    
    @IBAction func m2Btn(_ sender: Any)
    {
        viewThree.isHidden = false
        moreBtnThree.isHidden = false
        moreBtnTwo.isHidden = true
        viewThree.frame.origin.x = viewTwo.frame.origin.x
        viewThree.frame.origin.y = moreBtnTwo.frame.origin.y
        moreBtnThree.frame.origin.y = viewThree.frame.origin.y + 425
        
    }
    
    @IBAction func m3Btn(_ sender: Any)
    {
        viewFour.isHidden = false
        moreBtnFour.isHidden = false
        moreBtnThree.isHidden = true
        viewFour.frame.origin.x = viewTwo.frame.origin.x
        viewFour.frame.origin.y = moreBtnThree.frame.origin.y
        moreBtnFour.frame.origin.y = viewFour.frame.origin.y + 425
    }
    
    @IBAction func m4Btn(_ sender: Any)
    {
        viewFive.isHidden = false
        moreBtnFive.isHidden = false
        moreBtnFour.isHidden = true
        viewFive.frame.origin.x = viewTwo.frame.origin.x
        viewFive.frame.origin.y = moreBtnFour.frame.origin.y
        moreBtnFive.frame.origin.y = viewFive.frame.origin.y + 425
    }
    
    @IBAction func m5Btn(_ sender: Any)
    {
        viewSix.isHidden = false
        moreBtnSix.isHidden = false
        moreBtnFive.isHidden = true
        viewSix.frame.origin.x = viewTwo.frame.origin.x
        viewSix.frame.origin.y = moreBtnFive.frame.origin.y
        moreBtnSix.frame.origin.y = viewSix.frame.origin.y + 430
    }
    
    @IBAction func m6Btn(_ sender: Any)
    {
        viewSeven.isHidden = false
        moreBtnSeven.isHidden = false
        moreBtnSix.isHidden = true
        viewSeven.frame.origin.x = viewTwo.frame.origin.x
        viewSeven.frame.origin.y = moreBtnSix.frame.origin.y
        moreBtnSeven.frame.origin.y = viewSeven.frame.origin.y + 425
        
    }
    
    @IBAction func m7Btn(_ sender: Any)
    {
        viewEight.isHidden = false
        moreBtnEight.isHidden = false
        moreBtnSeven.isHidden = true
        viewEight.frame.origin.x = viewTwo.frame.origin.x
        viewEight.frame.origin.y = moreBtnSeven.frame.origin.y
        moreBtnEight.frame.origin.y = viewEight.frame.origin.y + 425
        
    }
    
    @IBAction func m8Btn(_ sender: Any)
    {
        viwNine.isHidden = false
        moreBtnEight.isHidden = true
        viwNine.frame.origin.x = viewTwo.frame.origin.x
        viwNine.frame.origin.y = moreBtnEight.frame.origin.y - 15
    }
    
    
    @IBAction func heightToBtn(_ sender: Any)
    {
        if listTableViewOne.isHidden
        {
            listTableViewOne.isHidden = false
            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 103
            listTableViewOne.frame.size.width = 350
            self.checkOne = "heightTo"
            listTableViewOne.reloadData()
        }
        else
        {
            listTableViewOne.isHidden = true
           
        }
    }
    
    @IBAction func heightFromBtn(_ sender: Any)
    {
        if listTableViewOne.isHidden
        {
            listTableViewOne.isHidden = false
            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 188
            listTableViewOne.frame.size.width = 350
            self.checkOne = "heightFrom"
            listTableViewOne.reloadData()
        }
        else
        {
            listTableViewOne.isHidden = true
        }
    }
    
    @IBAction func countryBtn(_ sender: Any)
    {
        if listTableViewOne.isHidden
        {
            listTableViewOne.isHidden = false
            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 270
            listTableViewOne.frame.size.width = 350
            self.checkOne = "country"
            self.GetData(urlStr: "countries", params: "", checkStr: "countryStr")
           
        }
        else
        {
            listTableViewOne.isHidden = true
        }
    }
    
    @IBAction func stateBtn(_ sender: Any)
    {
        if listTableViewOne.isHidden
        {
           // listTableViewOne.isHidden = false
            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 355
            listTableViewOne.frame.size.width = 350
            self.checkOne = "state"
            let parms = "id=\(self.country_id)"
            print("parms = \(parms)")
            self.GetData(urlStr: "state", params: parms , checkStr: "stateStr")
        }
        else
        {
            listTableViewOne.isHidden = true
        }
    }
    
    @IBAction func cityBtn(_ sender: Any)
    {
        if listTableViewOne.isHidden
        {
            listTableViewOne.isHidden = false
            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 433
            listTableViewOne.frame.size.width = 350
            self.checkOne = "city"
            let parms = "id=\(self.state_id)"
            print("state_id = \(parms)")
            self.GetData(urlStr: "city", params: parms , checkStr: "cityStr")
            
        }
        else
        {
            listTableViewOne.isHidden = true
        }
    }
    
    @IBAction func genderButton(_ sender: Any)
    {
        if listTableViewOne.isHidden
        {
            listTableViewOne.isHidden = false
            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 530
            listTableViewOne.frame.size.height = 130
            self.checkOne = "gender"
            listTableViewOne.reloadData()
        }
        else
        {
            listTableViewOne.isHidden = true
        }
    }
    
    @IBAction func haircolorButton(_ sender: Any)
    {
        if listTableViewOne.isHidden
        {
            listTableViewOne.isHidden = false
            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 612
            self.checkOne = "haircolor"
            let params = ""
            self.GetData(urlStr: "advance_searching_view", params: params, checkStr: "height")
        }
        else
        {
            listTableViewOne.isHidden = true
        }
    }
    
    @IBAction func hairLengthButton(_ sender: Any)
    {
        if listTableViewOne.isHidden
        {
            listTableViewOne.isHidden = false
            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 698
            listTableViewOne.frame.size.height = 325
            self.checkOne = "hairLength"
            let params = ""
            self.GetData(urlStr: "advance_searching_view", params: params, checkStr: "height")
        }
        else
        {
            listTableViewOne.isHidden = true
        }
    }
    
    @IBAction func hairTypeButton(_ sender: Any)
    {
        if listTableViewOne.isHidden
        {
            listTableViewOne.isHidden = false
            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 778
            self.checkOne = "hairType"
            let params = ""
            self.GetData(urlStr: "advance_searching_view", params: params, checkStr: "height")
        }
        else
        {
            listTableViewOne.isHidden = true
        }
    }
    
    @IBAction func eyeColorButton(_ sender: Any)
    {
        if listTableViewOne.isHidden
        {
            listTableViewOne.isHidden = false
            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 858
            self.checkOne = "eyeColor"
            let params = ""
            self.GetData(urlStr: "advance_searching_view", params: params, checkStr: "height")
        }
        else
        {
            listTableViewOne.isHidden = true
        }
    }
    
    @IBAction func eyeWearButton(_ sender: Any)
    {
        if listTableViewOne.isHidden
        {
            listTableViewOne.isHidden = false
            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 950
            self.checkOne = "eyeWear"
            let params = ""
            self.GetData(urlStr: "advance_searching_view", params: params, checkStr: "height")
        }
        else
        {
            listTableViewOne.isHidden = true
        }
    }
    
    @IBAction func bodyArtButton(_ sender: Any)
    {
        if listTableViewOne.isHidden
        {
            listTableViewOne.isHidden = false
            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 1035
            self.checkOne = "bodyArt"
            let params = ""
            self.GetData(urlStr: "advance_searching_view", params: params, checkStr: "height")
        }
        else
        {
            listTableViewOne.isHidden = true
        }
    }
    
    @IBAction func bodyTypeButton(_ sender: Any)
    {
        if listTableViewOne.isHidden
        {
            listTableViewOne.isHidden = false
            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 1120
            self.checkOne = "bodyType"
            let params = ""
            self.GetData(urlStr: "advance_searching_view", params: params, checkStr: "height")
        }
        else
        {
            listTableViewOne.isHidden = true
        }
    }
    
    @IBAction func ethnicityButton(_ sender: Any)
    {
        if listTableViewOne.isHidden
        {
            listTableViewOne.isHidden = false
            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 1205
            self.checkOne = "ethnicity"
            let params = ""
            self.GetData(urlStr: "advance_searching_view", params: params, checkStr: "height")
        }
        else
        {
            listTableViewOne.isHidden = true
        }
    }
    
    @IBAction func facialHairButton(_ sender: Any)
    {
        if listTableViewOne.isHidden
        {
            listTableViewOne.isHidden = false
            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 1280
            self.checkOne = "facialHair"
            let params = ""
            self.GetData(urlStr: "advance_searching_view", params: params, checkStr: "height")
        }
        else
        {
            listTableViewOne.isHidden = true
        }
    }
    
    @IBAction func featureButton(_ sender: Any)
    {
        if listTableViewOne.isHidden
        {
            listTableViewOne.isHidden = false
            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 1380
            self.checkOne = "feature"
            let params = ""
            self.GetData(urlStr: "advance_searching_view", params: params, checkStr: "height")
        }
        else
        {
            listTableViewOne.isHidden = true
        }
    }
    
    @IBAction func appereanceButton(_ sender: Any)
    {
        if listTableViewOne.isHidden
        {
            listTableViewOne.isHidden = false
            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 1465
            self.checkOne = "appereance"
            let params = ""
            self.GetData(urlStr: "advance_searching_view", params: params, checkStr: "height")
        }
        else
        {
            listTableViewOne.isHidden = true
        }
    }
    
    @IBAction func drinkButton(_ sender: Any)
    {
        if listTableViewOne.isHidden
        {
            listTableViewOne.isHidden = false
            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 1550
            self.checkOne = "drink"
            let params = ""
            self.GetData(urlStr: "advance_searching_view", params: params, checkStr: "height")
        }
        else
        {
            listTableViewOne.isHidden = true
        }
    }
    
    @IBAction func smokeButton(_ sender: Any)
    {
        if listTableViewOne.isHidden
        {
            listTableViewOne.isHidden = false
            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 1633
            self.checkOne = "smoke"
            let params = ""
            self.GetData(urlStr: "advance_searching_view", params: params, checkStr: "height")
        }
        else
        {
            listTableViewOne.isHidden = true
        }
    }
    
    @IBAction func maritalButton(_ sender: Any)
    {
        if listTableViewOne.isHidden
        {
            listTableViewOne.isHidden = false
            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 1710
            self.checkOne = "marital"
            let params = ""
            self.GetData(urlStr: "advance_searching_view", params: params, checkStr: "height")
        }
        else
        {
            listTableViewOne.isHidden = true
        }
    }
    
    @IBAction func childrenButton(_ sender: Any)
    {
        if listTableViewOne.isHidden
        {
            listTableViewOne.isHidden = false
            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 1805
            self.checkOne = "child"
            let params = ""
            self.GetData(urlStr: "advance_searching_view", params: params, checkStr: "height")
        }
        else
        {
            listTableViewOne.isHidden = true
        }
    }
    
    @IBAction func numberOfChildButton(_ sender: Any)
    {
//        if listTableViewOne.isHidden
//        {
//            listTableViewOne.isHidden = false
//            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
//            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 1800
//            self.checkOne = "noChild"
//            let params = ""
//            self.GetData(urlStr: "advance_searching_view", params: params, checkStr: "height")
//        }
//        else
//        {
//            listTableViewOne.isHidden = true
//        }
    }
    
    @IBAction func oldChildButton(_ sender: Any)
    {
//        if listTableViewOne.isHidden
//        {
//            listTableViewOne.isHidden = false
//            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
//            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 1800
//            self.checkOne = "oldChild"
//            let params = ""
//            self.GetData(urlStr: "advance_searching_view", params: params, checkStr: "height")
//        }
//        else
//        {
//            listTableViewOne.isHidden = true
//        }
    }
    
    @IBAction func youngChildButton(_ sender: Any)
    {
//        if listTableViewOne.isHidden
//        {
//            listTableViewOne.isHidden = false
//            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
//            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 1800
//            self.checkOne = "youngChild"
//            let params = ""
//            self.GetData(urlStr: "advance_searching_view", params: params, checkStr: "height")
//        }
//        else
//        {
//            listTableViewOne.isHidden = true
//        }
    }
    
    @IBAction func moreChildButton(_ sender: Any)
    {
//        if listTableViewOne.isHidden
//        {
//            listTableViewOne.isHidden = false
//            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
//            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 1800
//            self.checkOne = "moreChild"
//            let params = ""
//            self.GetData(urlStr: "advance_searching_view", params: params, checkStr: "height")
//        }
//        else
//        {
//            listTableViewOne.isHidden = true
//        }
    }
    
    @IBAction func petsButton(_ sender: Any)
    {
        if listTableViewOne.isHidden
        {
            listTableViewOne.isHidden = false
            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 2233
            self.checkOne = "pets"
            let params = ""
            self.GetData(urlStr: "advance_searching_view", params: params, checkStr: "height")
        }
        else
        {
            listTableViewOne.isHidden = true
        }
    }
    
    @IBAction func occupationButton(_ sender: Any)
    {
        if listTableViewOne.isHidden
        {
            listTableViewOne.isHidden = false
            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 2316
            self.checkOne = "occupation"
            let params = ""
            self.GetData(urlStr: "advance_searching_view", params: params, checkStr: "height")
        }
        else
        {
            listTableViewOne.isHidden = true
        }
    }
    
    @IBAction func employmentStatusButton(_ sender: Any)
    {
        if listTableViewOne.isHidden
        {
            listTableViewOne.isHidden = false
            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 2398
            self.checkOne = "employment"
            let params = ""
            self.GetData(urlStr: "advance_searching_view", params: params, checkStr: "height")
        }
        else
        {
            listTableViewOne.isHidden = true
        }
    }
    
    @IBAction func annualIncomeButton(_ sender: Any)
    {
        if listTableViewOne.isHidden
        {
            listTableViewOne.isHidden = false
            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 2480
            self.checkOne = "annual"
            let params = ""
            self.GetData(urlStr: "advance_searching_view", params: params, checkStr: "height")
        }
        else
        {
            listTableViewOne.isHidden = true
        }
    }
    
    @IBAction func homeTypeButton(_ sender: Any)
    {
        if listTableViewOne.isHidden
        {
            listTableViewOne.isHidden = false
            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 2560
            self.checkOne = "home"
            let params = ""
            self.GetData(urlStr: "advance_searching_view", params: params, checkStr: "height")
        }
        else
        {
            listTableViewOne.isHidden = true
        }
    }
    
    @IBAction func situationBtn(_ sender: Any)
    {
        if listTableViewOne.isHidden
        {
            listTableViewOne.isHidden = false
            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 2660
            self.checkOne = "living"
            let params = ""
            self.GetData(urlStr: "advance_searching_view", params: params, checkStr: "height")
        }
        else
        {
            listTableViewOne.isHidden = true
        }
    }
    
    @IBAction func realocateBtn(_ sender: Any)
    {
        if listTableViewOne.isHidden
        {
            listTableViewOne.isHidden = false
            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 2747
            self.checkOne = "realocate"
            let params = ""
            self.GetData(urlStr: "advance_searching_view", params: params, checkStr: "height")
        }
        else
        {
            listTableViewOne.isHidden = true
        }
    }
    
    @IBAction func relationshipBtn(_ sender: Any)
    {
        if listTableViewOne.isHidden
        {
            listTableViewOne.isHidden = false
            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 2824
            self.checkOne = "relationship"
            let params = ""
            self.GetData(urlStr: "advance_searching_view", params: params, checkStr: "height")
        }
        else
        {
            listTableViewOne.isHidden = true
        }
    }
    
    @IBAction func nationalityBtn(_ sender: Any)
    {
        if listTableViewOne.isHidden
        {
            listTableViewOne.isHidden = false
            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 2913
            self.checkOne = "countryOne"
            let params = ""
            self.GetData(urlStr: "countries", params: params, checkStr: "countryStr")
        }
        else
        {
            listTableViewOne.isHidden = true
        }
    }
    
    @IBAction func educationBtn(_ sender: Any)
    {
        if listTableViewOne.isHidden
        {
            listTableViewOne.isHidden = false
            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 2990
            self.checkOne = "education"
            let params = ""
            self.GetData(urlStr: "advance_searching_view", params: params, checkStr: "height")
        }
        else
        {
            listTableViewOne.isHidden = true
        }
    }
    
    @IBAction func spokenButton(_ sender: Any)
    {
        if listTableViewOne.isHidden
        {
            listTableViewOne.isHidden = false
            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 3090
            self.checkOne = "spoken"
            let params = ""
            self.GetData(urlStr: "advance_searching_view", params: params, checkStr: "height")
        }
        else
        {
            listTableViewOne.isHidden = true
        }
    }
    
    @IBAction func abilityButton(_ sender: Any)
    {
        if listTableViewOne.isHidden
        {
            listTableViewOne.isHidden = false
            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 3168
            self.checkOne = "ability"
            let params = ""
            self.GetData(urlStr: "advance_searching_view", params: params, checkStr: "height")
        }
        else
        {
            listTableViewOne.isHidden = true
        }
    }
    
    @IBAction func religionButon(_ sender: Any)
    {
        if listTableViewOne.isHidden
        {
            listTableViewOne.isHidden = false
            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 3248
            self.checkOne = "religion"
            let params = ""
            self.GetData(urlStr: "advance_searching_view", params: params, checkStr: "height")
        }
        else
        {
            listTableViewOne.isHidden = true
        }
    }
    
    @IBAction func starSignButton(_ sender: Any)
    {
        if listTableViewOne.isHidden
        {
            listTableViewOne.isHidden = false
            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 3335
            self.checkOne = "star"
            let params = ""
            self.GetData(urlStr: "advance_searching_view", params: params, checkStr: "height")
        }
        else
        {
            listTableViewOne.isHidden = true
        }
    }
    
    @IBAction func funButton(_ sender: Any)
    {
        if listTableViewOne.isHidden
        {
            listTableViewOne.isHidden = false
            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 3413
            self.checkOne = "fun"
            let params = ""
            self.GetData(urlStr: "advance_searching_view", params: params, checkStr: "height")
        }
        else
        {
            listTableViewOne.isHidden = true
        }
    }
    
    @IBAction func foodButton(_ sender: Any)
    {
        if listTableViewOne.isHidden
        {
            listTableViewOne.isHidden = false
            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 3495
            self.checkOne = "food"
            let params = ""
            self.GetData(urlStr: "advance_searching_view", params: params, checkStr: "height")
        }
        else
        {
            listTableViewOne.isHidden = true
        }
    }
    
    @IBAction func musicButton(_ sender: Any)
    {
        if listTableViewOne.isHidden
        {
            listTableViewOne.isHidden = false
            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 3510
            self.checkOne = "music"
            let params = ""
            self.GetData(urlStr: "advance_searching_view", params: params, checkStr: "height")
        }
        else
        {
            listTableViewOne.isHidden = true
        }
    }
    
    @IBAction func playButton(_ sender: Any)
    {
        if listTableViewOne.isHidden
        {
            listTableViewOne.isHidden = false
            listTableViewOne.frame.origin.x = self.view.frame.origin.x + 4
            listTableViewOne.frame.origin.y = self.view.frame.origin.y + 3490
            self.checkOne = "play"
            let params = ""
            self.GetData(urlStr: "advance_searching_view", params: params, checkStr: "height")
        }
        else
        {
            listTableViewOne.isHidden = true
        }
    }
    
    @IBAction func saveBtn(_ sender: Any) {
        
       let params = "haircolor=\((hairColorBtn.titleLabel?.text)!)&hairlength=\((hairLengthBtn.titleLabel?.text)!)&hairtype=\((hairTypeBtn.titleLabel?.text)!)&eyecolor=\((eyeColorBtn.titleLabel?.text)!)&eyewear=\((eyeWearBtn.titleLabel?.text)!)&body_type=\((bodyTypeBtn.titleLabel?.text)!)ðnicity=\((ethnicityBtn.titleLabel?.text)!)&bodyArt=\((bodyArtBtn.titleLabel?.text)!)&appearance=\((appereanceBtn.titleLabel?.text)!)&bestFeature=\((featureBtn.titleLabel?.text)!)&starSign=\((starSignBtn.titleLabel?.text)!)&drink=\((drinkBtn.titleLabel?.text)!)&smoke=\((smokeBtn.titleLabel?.text)!)&facialHair=\((facialHairBtn.titleLabel?.text)!)&occupation=\((occupationBtn.titleLabel?.text)!)&childrenHave=\((childrenBtn.titleLabel?.text)!)&pets=\((petsBtn.titleLabel?.text)!)&employmentStatus=\((employmentStatusBtn.titleLabel?.text)!)&homeType=\((homeTypeBtn.titleLabel?.text)!)&education=\((educationBtn.titleLabel?.text)!)&incomeBracket=\((annualIncomeBtn.titleLabel?.text)!)&religion=\((religionBtn.titleLabel?.text)!)&nationality=\((nationalityBtn.titleLabel?.text)!)&language_spoken=\((langSpokenBtn.titleLabel?.text)!)&food_like=\((foodBtn.titleLabel?.text)!)&entertainet=\((funBtn.titleLabel?.text)!)&like_music=\((musicBtn.titleLabel?.text)!)&country=\((countryBtn.titleLabel?.text)!)&city=\((cityBtn.titleLabel?.text)!)&state=\((stateBtn.titleLabel?.text)!)&relocate=\((realocateBtn.titleLabel?.text)!)&englishAbility=\((langAbilityBtn.titleLabel?.text)!)&livingSituation=\((situationBtn.titleLabel?.text)!)&height_from=\((heightFrom.titleLabel?.text)!)&height_to=\((heightToBtn.titleLabel?.text)!)&play_sports=\((playBtn.titleLabel?.text)!)&age_from=\(minAdvanceLbl.text!)&age_to=\(maxAdvanceLbl.text!)&gender=\((genderBtn.titleLabel?.text)!)&page=0&maritalStatus=\((maritalBtn.titleLabel?.text)!)&user_id=27730895"
        self.GetData(urlStr: "advance_searching", params: params, checkStr: "saveStr")
    }
    
    
    func GetData(urlStr : String , params : String , checkStr : String){
        // LoadingIndicatorView.show()
        
        ApiResponse.onResponsePostPhp(url: urlStr, parms: params, completion: { (result, error) in
            
            if (error == "") {
                
                let status = result["status"] as! Bool
                let message = result["message"] as! String
                if (status == true) {
                    
                    OperationQueue.main.addOperation {
                        LoadingIndicatorView.hide()
                        
                        if(checkStr == "height")
                        {
                            let heightDict = result["data"] as! NSDictionary
                            self.heighrToArr = heightDict["height_from"] as! [NSDictionary]
                            for dict in self.heighrToArr
                            {
                                self.heightToList.append(dict["height_values"] as! String)
                            }
                            
                            self.hairColorArr = heightDict["hair_color"] as! [NSDictionary]
                            for hairColorDice in self.hairColorArr
                            {
                                self.hairColorLis.append(hairColorDice["hair_color_values"] as! String)
                            }
                            
                            self.hairLengthArr = heightDict["hair_length"] as! [NSDictionary]
                            for hairLengthDict in self.hairLengthArr
                            {
                                self.hairLengthList.append(hairLengthDict["hair_length_values"] as! String)
                            }
                            
                            self.hairTypeArr = heightDict["hair_type"] as! [NSDictionary]
                            for hairTypeDict in self.hairTypeArr
                            {
                                self.hairTypeList.append(hairTypeDict["hair_type_values"] as! String)
                            }
                            
                            self.eyeColorArr = heightDict["eye_color"] as! [NSDictionary]
                            for eyeDict in self.eyeColorArr
                            {
                                self.eyeColorList.append(eyeDict["eye_color_values"] as! String)
                            }
                            
                            self.eyeWearArr = heightDict["eye_wear"] as! [NSDictionary]
                            for eyeDict in self.eyeWearArr
                            {
                                self.eyeWearList.append(eyeDict["eye_wear_values"] as! String)
                            }
                            
                            self.bodyArtArr = heightDict["body_art"] as! [NSDictionary]
                            for bodyDict in self.bodyArtArr
                            {
                                self.bodyArtList.append(bodyDict["body_art_values" ] as! String)
                            }
                            
                            self.bodyTypeArr = heightDict["body_type"] as! [NSDictionary]
                            for bDict in self.bodyTypeArr
                            {
                                self.bodyTypeList.append(bDict["body_type_values"] as! String)
                            }
                            
                            self.ethnicityArr = heightDict["ethnicity"] as! [NSDictionary]
                            for eDict in self.ethnicityArr
                            {
                                self.ethnicityList.append(eDict["ethnicity_values"] as! String)
                            }
                            
                            self.facialArr = heightDict["facialHair"] as! [NSDictionary]
                            for fDict in self.facialArr
                            {
                                self.facialList.append(fDict["facialHair_values"] as! String)
                            }
                            
                            self.featureArr = heightDict["best_feature"] as! [NSDictionary]
                            for fDict in self.featureArr
                            {
                                self.featureList.append(fDict["best_feature_values"] as! String)
                            }
                            
                            self.appereanceArr = heightDict["appearance"] as! [NSDictionary]
                            for aDict in self.appereanceArr
                            {
                                self.appereanceList.append(aDict["appearance_values"] as! String)
                            }
                            
                            self.drinkArr = heightDict["drink"] as! [NSDictionary]
                            for dDict in self.drinkArr
                            {
                                self.drinkList.append(dDict["drink_values"] as! String)
                            }
                            
                            self.smokeArr = heightDict["smoke"] as! [NSDictionary]
                            for sDict in self.smokeArr
                            {
                                self.smokeList.append(sDict["smoke_values"] as! String)
                            }
                            
                            self.maritalArr = heightDict["marital_status"] as! [NSDictionary]
                            for mDict in self.maritalArr
                            {
                                self.maritalList.append(mDict["maritalstatus_val_values"] as! String)
                            }
                            
                            self.childrenArr = heightDict["have_children"] as! [NSDictionary]
                            for cDict in self.childrenArr
                            {
                                self.childrenList.append(cDict["childrenHave_values"] as! String)
                            }
                            
                            // number of children section hide
                            // old child hide
                            // young child hide
                            // more child hide
                            
                            self.petsArr = heightDict["havePets"] as! [NSDictionary]
                            for pDict in self.petsArr
                            {
                                self.petsList.append(pDict["havePets_values"] as! String)
                            }
                            
                            self.occupationArr = heightDict["occupation"] as! [NSDictionary]
                            for oDict in self.occupationArr
                            {
                                self.occupationList.append(oDict["occupation_values"] as! String)
                            }
                            
                            self.employmentStatusArr = heightDict["employmentStatus"] as! [NSDictionary]
                            for eDict in self.employmentStatusArr
                            {
                                self.employmentStatusList.append(eDict["employmentStatus_values"] as! String)
                            }
                            
                            self.annualIncomeArr = heightDict["incomeBracket"] as! [NSDictionary]
                            for aDict in self.annualIncomeArr
                            {
                                self.annualIncomeList.append(aDict["incomeBracket_values"] as! String)
                            }
                            
                            self.homeTypeArr = heightDict["homeType"] as! [NSDictionary]
                            for hDict in self.homeTypeArr
                            {
                                self.homeTYpeList.append(hDict["homeType_values"] as! String)
                            }
                            
                            self.situationArr = heightDict["livingSituation"] as! [NSDictionary]
                            for sDict in self.situationArr
                            {
                                self.situationList.append(sDict["livingSituation_values"] as! String)
                            }
                            
                            self.realocateArr = heightDict["relocate"] as! [NSDictionary]
                            for rDict in self.realocateArr
                            {
                                self.realocateList.append(rDict["relocate_values"] as! String)
                            }
                            
                            self.relationShipArr = heightDict["relationships"] as! [NSDictionary]
                            for rDict in self.relationShipArr
                            {
                                self.relationshipList.append(rDict["relationship_val"] as! String)
                            }
                            
                            self.educationArr = heightDict["education"] as! [NSDictionary]
                            for eDict in self.educationArr
                            {
                                self.educationList.append(eDict["education_val"] as! String)
                            }
                            
                            // ------
                            
                            self.langSpokenArr = heightDict["language_spoken"] as! [NSDictionary]
                            for lDict in self.langSpokenArr
                            {
                                self.langSpokenList.append(lDict["languagess_values"] as! String)
                            }
                            
                            self.langAbilityArr = heightDict["english_ability"] as! [NSDictionary]
                            for laDict in self.langAbilityArr
                            {
                                self.langAbilityList.append(laDict["englishAbility_values"] as! String)
                            }
                            
                            self.religionArr = heightDict["religion"] as! [NSDictionary]
                            for rDict in self.religionArr
                            {
                                self.religionList.append(rDict["religion_values"] as! String)
                            }
                            
                            self.starSignArr = heightDict["starSign"] as! [NSDictionary]
                            for sDict in self.starSignArr
                            {
                                self.starSignList.append(sDict["starSign_values"] as! String)
                            }
                            
                            self.funArr = heightDict["entertainment"] as! [NSDictionary]
                            for fDict in self.funArr
                            {
                                self.funList.append(fDict["entertainment_values"] as! String)
                            }
                            
                            self.foodArr = heightDict["food_like"] as! [NSDictionary]
                            for fDict in self.foodArr
                            {
                                self.foodList.append(fDict["food_like_values"] as! String)
                            }
                            
                            self.musicArr = heightDict["like_music"] as! [NSDictionary]
                            for mDict in self.musicArr
                            {
                                self.musicList.append(mDict["like_music_values"] as! String)
                            }
                            
                            self.playArr = heightDict["play_sports"] as! [NSDictionary]
                            for pDict in self.playArr
                            {
                                self.playList.append(pDict["play_sports_values"] as! String)
                            }
                            
                            self.listTableViewOne.reloadData()
                            self.view.layoutIfNeeded()
                            
                        }
                        else if (checkStr == "countryStr")
                        {
                            self.countryArrOne = result["data"] as! [NSDictionary]
                            for countryDict in self.countryArrOne
                            {
                                self.countryList.append(countryDict["name"] as! String)
                            }
                            self.listTableViewOne.reloadData()
                        }
                        else if(checkStr == "stateStr")
                        {
                            self.stateArr = result["data"] as! [NSDictionary]
                            print("stateArr = \(self.stateArr)")
                            for dict1 in self.stateArr
                            {
                                self.stateList.append(dict1["name"] as! String)
                                print("stateList == \(self.stateList)")
                            }
                            self.listTableViewOne.isHidden = false
                            self.listTableViewOne.reloadData()
                        }
                        else if (checkStr == "cityStr")
                        {
                            self.cityArr = result["data"] as! [NSDictionary]
                            for cityDict in self.cityArr
                            {
                                self.cityList.append(cityDict["name"] as! String)
                            }
                            
                            self.listTableViewOne.reloadData()
                            
                        }
                        else if (checkStr == "save")
                        {
                            let dict = result["data"] as! NSDictionary
                            ApiResponse.alert(title: "", message: message, controller: self)
                        }
                    }
                }
                else
                {
                    ApiResponse.alert(title: "Ooooops", message: message, controller: self)
                }
            }
            else
            {
                if (error == Constant.Status_Not_200)
                {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else
                {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        })
    }

    

}
extension AdvanceSearchVC : UITableViewDataSource , UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(checkOne == "heightTo") || (checkOne == "heightFrom")
        {
            return heighrToArr.count
        }
        else if (checkOne == "country")
        {
            return countryArrOne.count
        }
        else if (checkOne == "countryOne")
        {
            return countryArrOne.count
        }
        else if (checkOne == "state")
        {
            return stateArr.count
        }
        else if (checkOne == "city")
        {
            return cityArr.count
        }
        else if (checkOne == "gender")
        {
            return genderListOne.count
        }
        else if (checkOne == "haircolor")
        {
            return hairColorArr.count
        }
        else if (checkOne == "hairLength")
        {
            return hairLengthArr.count
        }
        else if (checkOne == "hairType")
        {
            return hairTypeArr.count
        }
        else if (checkOne == "eyeColor")
        {
            return eyeColorArr.count
        }
        else if (checkOne == "eyeWear")
        {
            return eyeWearArr.count
        }
        else if (checkOne == "bodyArt")
        {
            return bodyArtArr.count
        }
        else if (checkOne == "bodyType")
        {
            return bodyTypeArr.count
        }
        else if (checkOne == "ethnicity")
        {
            return ethnicityArr.count
        }
        else if (checkOne == "facialHair")
        {
            return facialArr.count
        }
        else if (checkOne == "feature")
        {
            return featureArr.count
        }
        else if (checkOne == "appereance")
        {
            return appereanceArr.count
        }
        else if (checkOne == "drink")
        {
            return drinkArr.count
        }
        else if (checkOne == "smoke")
        {
            return smokeArr.count
        }
        else if (checkOne == "marital")
        {
            return maritalArr.count
        }
        else if (checkOne == "child")
        {
            return childrenArr.count
        }
        else if (checkOne == "pets")
        {
            return petsArr.count
        }
        else if (checkOne == "occupation")
        {
            return occupationArr.count
        }
        else if (checkOne == "employment")
        {
            return employmentStatusArr.count
        }
        else if (checkOne == "annual")
        {
            return annualIncomeArr.count
        }
        else if (checkOne == "home")
        {
            return homeTypeArr.count
        }
        else if (checkOne == "living")
        {
            return situationArr.count
        }
        else if (checkOne == "realocate")
        {
            return realocateArr.count
        }
        else if (checkOne == "relationship")
        {
            return relationShipArr.count
        }
        else if (checkOne == "education")
        {
            return educationArr.count
        }
        else if (checkOne == "spoken")
        {
            return langSpokenArr.count
        }
        else if (checkOne == "ability")
        {
            return langAbilityArr.count
        }
        else if (checkOne == "religion")
        {
            return religionArr.count
        }
        else if (checkOne == "star")
        {
            return starSignArr.count
        }
        else if (checkOne == "fun")
        {
            return funArr.count
        }
        else if (checkOne == "food")
        {
            return foodArr.count
        }
        else if (checkOne == "music")
        {
            return musicArr.count
        }
        else
        {
            return playArr.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : ListOneCell = listTableViewOne.dequeueReusableCell(withIdentifier: "listonecell") as! ListOneCell
        
        if(checkOne == "heightTo") || (checkOne == "heightFrom")
        {
         cell.listNameLabel.text! = heightToList[indexPath.row]
        }
        else if (checkOne == "country")
        {
            if (countryList.count > 0)
            {
                cell.listNameLabel.text! = countryList[indexPath.row]
            }
        }
        else if (checkOne == "countryOne")
        {
            if (countryList.count > 0)
            {
                cell.listNameLabel.text! = countryList[indexPath.row]
            }
        }
        else if (checkOne == "state")
        {
            print("state == \(stateList.count) -- \(indexPath.row)")
            if (stateList.count > 0)
            {
                 cell.listNameLabel.text! = stateList[indexPath.row]
            }
        }
        else if (checkOne == "city")
        {
            if (cityList.count > 0)
            {
                cell.listNameLabel.text! = cityList[indexPath.row]
            }
        }
        else if (checkOne == "gender")
        {
            if (genderListOne.count > 0)
            {
                cell.listNameLabel.text! = genderListOne[indexPath.row]
            }
        }
        else if (checkOne == "haircolor")
        {
            if (hairColorLis.count > 0)
            {
                cell.listNameLabel.text! = hairColorLis[indexPath.row]
            }
        }
        else if (checkOne == "hairLength")
        {
            if (hairLengthList.count > 0)
            {
                cell.listNameLabel.text! = hairLengthList[indexPath.row]
            }
        }
        else if (checkOne == "hairType")
        {
            if (hairTypeList.count > 0)
            {
                cell.listNameLabel.text! = hairTypeList[indexPath.row]
            }
        }
        else if (checkOne == "eyeColor")
        {
            if (eyeColorList.count > 0)
            {
                cell.listNameLabel.text! = eyeColorList[indexPath.row]
            }
        }
        else if (checkOne == "eyeWear")
        {
            if (eyeWearList.count > 0)
            {
                cell.listNameLabel.text! = eyeWearList[indexPath.row]
            }
        }
        else if (checkOne == "bodyArt")
        {
            if (bodyArtList.count > 0)
            {
                cell.listNameLabel.text! = bodyArtList[indexPath.row]
            }
        }
        else if (checkOne == "bodyType")
        {
            if (bodyTypeList.count > 0)
            {
                cell.listNameLabel.text! = bodyTypeList[indexPath.row]
            }
        }
        else if (checkOne == "ethnicity")
        {
            if (ethnicityList.count > 0)
            {
                cell.listNameLabel.text! = ethnicityList[indexPath.row]
            }
        }
        else if (checkOne == "facialHair")
        {
            if (facialList.count > 0)
            {
                cell.listNameLabel.text! = facialList[indexPath.row]
            }
        }
        else if (checkOne == "feature")
        {
            if (featureList.count > 0)
            {
                cell.listNameLabel.text! = featureList[indexPath.row]
            }
        }
        else if (checkOne == "appereance")
        {
            if (appereanceList.count > 0)
            {
                cell.listNameLabel.text! = appereanceList[indexPath.row]
            }
        }
        else if (checkOne == "drink")
        {
            if (drinkList.count > 0)
            {
                cell.listNameLabel.text! = drinkList[indexPath.row]
            }
        }
        else if (checkOne == "smoke")
        {
            if (smokeList.count > 0)
            {
                cell.listNameLabel.text! = smokeList[indexPath.row]
            }
        }
        else if (checkOne == "marital")
        {
            if (maritalList.count > 0)
            {
                cell.listNameLabel.text! = maritalList[indexPath.row]
            }
        }
        else if (checkOne == "child")
        {
            if (childrenList.count > 0)
            {
                cell.listNameLabel.text! = childrenList[indexPath.row]
            }
        }
        else if (checkOne == "pets")
        {
            if (petsList.count > 0)
            {
                cell.listNameLabel.text! = petsList[indexPath.row]
            }
        }
        else if (checkOne == "occupation")
        {
            if (occupationList.count > 0)
            {
                cell.listNameLabel.text! = occupationList[indexPath.row]
            }
        }
        else if (checkOne == "employment")
        {
            if (employmentStatusList.count > 0)
            {
                cell.listNameLabel.text! = employmentStatusList[indexPath.row]
            }
        }
        else if (checkOne == "annual")
        {
            if (annualIncomeList.count > 0)
            {
                cell.listNameLabel.text! = annualIncomeList[indexPath.row]
            }
        }
        else if (checkOne == "home")
        {
            if (homeTYpeList.count > 0)
            {
                cell.listNameLabel.text! = homeTYpeList[indexPath.row]
            }
        }
        else if (checkOne == "living")
        {
            if (situationList.count > 0)
            {
                cell.listNameLabel.text! = situationList[indexPath.row]
            }
        }
        else if (checkOne == "realocate")
        {
            if (realocateList.count > 0)
            {
                cell.listNameLabel.text! = realocateList[indexPath.row]
            }
        }
        else if (checkOne == "relationship")
        {
            if (relationshipList.count > 0)
            {
                cell.listNameLabel.text! = relationshipList[indexPath.row]
            }
        }
        else if (checkOne == "education")
        {
            if (educationList.count > 0)
            {
                cell.listNameLabel.text! = educationList[indexPath.row]
            }
        }
        else if (checkOne == "spoken")
        {
            if (langSpokenList.count > 0)
            {
                cell.listNameLabel.text! = langSpokenList[indexPath.row]
            }
        }
        else if (checkOne == "ability")
        {
            if (langAbilityList.count > 0)
            {
                cell.listNameLabel.text! = langAbilityList[indexPath.row]
            }
        }
        else if (checkOne == "religion")
        {
            if (religionList.count > 0)
            {
                cell.listNameLabel.text! = religionList[indexPath.row]
            }
        }
        else if (checkOne == "star")
        {
            if (starSignList.count > 0)
            {
                cell.listNameLabel.text! = starSignList[indexPath.row]
            }
        }
        else if (checkOne == "fun")
        {
            if (funList.count > 0)
            {
                cell.listNameLabel.text! = funList[indexPath.row]
            }
        }
        else if (checkOne == "food")
        {
            if (foodList.count > 0)
            {
                cell.listNameLabel.text! = foodList[indexPath.row]
            }
        }
        else if (checkOne == "music")
        {
            if (musicList.count > 0)
            {
                cell.listNameLabel.text! = musicList[indexPath.row]
            }
        }
        else
        {
            if (playList.count > 0)
            {
                cell.listNameLabel.text! = playList[indexPath.row]
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        listTableViewOne.isHidden = true
        if(checkOne == "heightTo")
        {
            heightToBtn.setTitle(heightToList[indexPath.row], for: .normal)
        }
       else if(checkOne == "heightFrom")
        {
            heightFrom.setTitle(heightToList[indexPath.row], for: .normal)
        }
        else if(checkOne == "country")
        {
            let d1 = countryArrOne[indexPath.row]
            self.country_id = (d1["id"] as? String)!
            countryBtn.setTitle(countryList[indexPath.row], for: .normal)
        }
        else if(checkOne == "countryOne")
        {
            let d1 = countryArrOne[indexPath.row]
            self.country_id = (d1["id"] as? String)!
            nationalityBtn.setTitle(countryList[indexPath.row], for: .normal)
        }
        else if (checkOne == "state")
        {
            let s1 = stateArr[indexPath.row]
            self.state_id = s1["id"] as! String
            stateBtn.setTitle(stateList[indexPath.row], for: .normal)
        }
        else if (checkOne == "city")
        {
            cityBtn.setTitle(cityList[indexPath.row], for: .normal)
        }
        else if (checkOne == "gender")
        {
            genderBtn.setTitle(genderListOne[indexPath.row], for: .normal)
        }
        else if (checkOne == "haircolor")
        {
            hairColorBtn.setTitle(hairColorLis[indexPath.row], for: .normal)
        }
        else if (checkOne == "hairLength")
        {
            hairLengthBtn.setTitle(hairLengthList[indexPath.row], for: .normal)
        }
        else if (checkOne == "hairType")
        {
            hairTypeBtn.setTitle(hairTypeList[indexPath.row], for: .normal)
        }
        else if (checkOne == "eyeColor")
        {
            eyeColorBtn.setTitle(eyeColorList[indexPath.row], for: .normal)
        }
        else if (checkOne == "eyeWear")
        {
            eyeWearBtn.setTitle(eyeWearList[indexPath.row], for: .normal)
        }
        else if (checkOne == "bodyArt")
        {
            bodyArtBtn.setTitle(bodyArtList[indexPath.row], for: .normal)
        }
        else if (checkOne == "bodyType")
        {
            bodyTypeBtn.setTitle(bodyTypeList[indexPath.row], for: .normal)
        }
        else if (checkOne == "ethnicity")
        {
            ethnicityBtn.setTitle(ethnicityList[indexPath.row], for: .normal)
        }
        else if (checkOne == "facialHair")
        {
            facialHairBtn.setTitle(facialList[indexPath.row], for: .normal)
        }
        else if (checkOne == "feature")
        {
            featureBtn.setTitle(featureList[indexPath.row], for: .normal)
        }
        else if (checkOne == "appereance")
        {
            appereanceBtn.setTitle(appereanceList[indexPath.row], for: .normal)
        }
        else if (checkOne == "drink")
        {
            drinkBtn.setTitle(drinkList[indexPath.row], for: .normal)
        }
        else if (checkOne == "smoke")
        {
            smokeBtn.setTitle(smokeList[indexPath.row], for: .normal)
        }
        else if (checkOne == "marital")
        {
            maritalBtn.setTitle(maritalList[indexPath.row], for: .normal)
        }
        else if (checkOne == "child")
        {
            childrenBtn.setTitle(childrenList[indexPath.row], for: .normal)
        }
        else if (checkOne == "pets")
        {
            petsBtn.setTitle(petsList[indexPath.row], for: .normal)
        }
        else if (checkOne == "occupation")
        {
            occupationBtn.setTitle(occupationList[indexPath.row], for: .normal)
        }
        else if (checkOne == "employment")
        {
            employmentStatusBtn.setTitle(employmentStatusList[indexPath.row], for: .normal)
        }
        else if (checkOne == "annual")
        {
            annualIncomeBtn.setTitle(annualIncomeList[indexPath.row], for: .normal)
        }
        else if (checkOne == "home")
        {
            homeTypeBtn.setTitle(homeTYpeList[indexPath.row], for: .normal)
        }
        else if (checkOne == "living")
        {
            situationBtn.setTitle(situationList[indexPath.row], for: .normal)
        }
        else if (checkOne == "realocate")
        {
            realocateBtn.setTitle(realocateList[indexPath.row], for: .normal)
        }
        else if (checkOne == "relationship")
        {
            relationshipBtn.setTitle(relationshipList[indexPath.row], for: .normal)
        }
        else if (checkOne == "education")
        {
            educationBtn.setTitle(educationList[indexPath.row], for: .normal)
        }
        else if (checkOne == "spoken")
        {
            langSpokenBtn.setTitle(langSpokenList[indexPath.row], for: .normal)
        }
        else if (checkOne == "ability")
        {
            langAbilityBtn.setTitle(langAbilityList[indexPath.row], for: .normal)
        }
        else if (checkOne == "religion")
        {
            religionBtn.setTitle(religionList[indexPath.row], for: .normal)
        }
        else if (checkOne == "star")
        {
            starSignBtn.setTitle(starSignList[indexPath.row], for: .normal)
        }
        else if (checkOne == "fun")
        {
            funBtn.setTitle(funList[indexPath.row], for: .normal)
        }
        else if (checkOne == "food")
        {
            foodBtn.setTitle(foodList[indexPath.row], for: .normal)
        }
        else if (checkOne == "music")
        {
            musicBtn.setTitle(musicList[indexPath.row], for: .normal)
        }
        else if (checkOne == "play")
        {
            playBtn.setTitle(playList[indexPath.row], for: .normal)
        }
       
        
        
    }
}

class ListOneCell : UITableViewCell
{
    @IBOutlet weak var listNameLabel: UILabel!
    
}
