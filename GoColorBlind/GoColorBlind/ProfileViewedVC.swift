//
//  FilterUserVC.swift
//  GoColorBlind
//
//  Created by Prashant Shinde on 11/23/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit

class FilterUserVC: UIViewController {
    //***********************
    //MARK:- Properties
    //***********************
    @IBOutlet weak var tblIgnored: UITableView!

    var dictaryIgnore : [NSDictionary] = []
    var strApiParam : String = ""
    var intPageLoad = 1
    //***********************
    //MARK:- Application Life Cycle
    //***********************
    
    override func viewDidLoad() {
        super.viewDidLoad()

    SettingView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func SettingView() {
        
        Ignored_Api()
        
        self.tblIgnored?.tableFooterView = UIView ()
        tblIgnored.delegate = self
        tblIgnored.dataSource = self
        tblIgnored.register(UINib(nibName: "IgnoredTVCell", bundle: nil), forCellReuseIdentifier: "IgnoredTVCell")
        
        // Do any additional setup after loading the view.
    }
    
    //***********************
    //MARK:- Ignored_Api
    //***********************
    
    func Ignored_Api()
    {
        LoadingIndicatorView.show()
        let strpageload = String(intPageLoad)
        let params = "user_id=\(strUserid)&page=\(strpageload)"
        print("Ignored_Api params == \(params)")
        strApiParam = "block_user_listing"
        ApiResponse.onResponsePostPhp(url: strApiParam, parms: params, completion: { (result, error) in
            
            print("Ignored_Api response == \(result)")
            
            if (error == "") {
                let status = result["status"] as! Bool
                let message = result["message"] as! String
                if (status == true)
                {
                    OperationQueue.main.addOperation {
                        LoadingIndicatorView.hide()
                        self.dictaryIgnore = (result["data"] as! NSArray) as! [NSDictionary]
                        self.tblIgnored.reloadData()
                    }
                }
                else {
                    ApiResponse.alert(title: "Ooooops", message: message, controller: self)
                }
            }
            else {
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        })
    }
    
}


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */




//***********************
//MARK:- UITableView
//***********************

extension FilterUserVC : UITableViewDataSource , UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblIgnored{
            return dictaryIgnore.count
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
        let cell : IgnoredTVCell = tblIgnored.dequeueReusableCell(withIdentifier: "IgnoredTVCell") as! IgnoredTVCell
        
        let lastRowIndex = tblIgnored.numberOfRows(inSection: tableView.numberOfSections)
        
        if (indexPath.row == lastRowIndex) {
            print("last row selected")
            print("page load :\(intPageLoad)")
            intPageLoad += 1
            Ignored_Api()
        }
        
        if indexPath.row == lastRowIndex{
            print("page load :\(intPageLoad)")
            intPageLoad += 1
            Ignored_Api()
            
        }
        
            let orderDetail = self.dictaryIgnore[indexPath.row]
            
            if self.dictaryIgnore.count != 0 {
                let contactDict: NSDictionary = orderDetail
                
                // ---------- religions  language---------
                
                let  strhindu = contactDict["religions"] as? String!
                let strHindi = contactDict["language"] as? String!
                
                if (strhindu != nil){
                    cell.lblRelagion.text = strhindu
                };
                if (strHindi != nil){
                    cell.lblRelagion.text = strHindi!
                };if (strhindu != nil) && (strHindi != nil){
                    cell.lblRelagion.text = strHindi! + " " + strhindu!
                    
                }
                
                // ------- left image ---------
                
                let imageString = contactDict["main_profile_image"] as? String
                if imageString == ""{
                    cell.imgUserImage.image = UIImage(named: "female_photo.png")
                    print("Nulll")
                }else{
                    let imgUrl = ImgBase_Url + imageString!
                    cell.imgUserImage.sd_setImage(with: URL(string : imgUrl)! as URL)
                }
                
                //-----------first_name last_name  ---------
                
                let strfname = contactDict["first_name"] as? String
                let strlName = contactDict["last_name"] as? String
                cell.lblName.text = strfname! + " " + strlName!
                
                //--------------- age -----------------
                
                let str1 = contactDict["age"] as? String!
                let str2 = contactDict["heights"] as? String!
                let str3 = contactDict["religions"] as? String!
                cell.lblAge.text = str1!+","+str2!+" "+str3!
                
                // ---------- address---------
                
                cell.lblAddress.text =  contactDict["address"] as? String
            }else
            {
                let cell : NullDataTVCell = tblIgnored.dequeueReusableCell(withIdentifier: "IgnoredTVCell") as! NullDataTVCell
                return cell
            }
        
            return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      //  let dict = dictaryIgnore[indexPath.row]
        let Userdetailvw = self.storyboard?.instantiateViewController(withIdentifier: "DetailUserVC")as! DetailUserVC
        Userdetailvw.aryDisc = dictaryIgnore
        Userdetailvw.button_index_R = indexPath.row
        self.present(Userdetailvw, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == tblIgnored
        {
            
            if self.dictaryIgnore.count != 0
            {
                return 87
            }else{
                return 600
            }
        }else{
            return 0
        }
        
    }
    
    
    
    
}

