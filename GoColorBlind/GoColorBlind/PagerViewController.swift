//
//  PagerViewController.swift
//  GoColorBlind
//
//  Created by Prashant Shinde on 11/10/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit

class PagerViewController: UIViewController, UIScrollViewDelegate {

    
    //***********************
    //MARK:- Properties
    //***********************
    
    //@IBOutlet var textView: UIView!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var btnRegister: UIButton!
    @IBOutlet var btnSignIn: UIButton!
    @IBOutlet var textView: UITextView!
    @IBOutlet var textView2: UITextView!
    @IBOutlet var textView3: UITextView!


    
    override func viewDidLoad() {
        super.viewDidLoad()
        textView2.isHidden = true
        textView3.isHidden = true
        
//        UIView.animate(withDuration: 1.0, animations: { () -> Void in
//            self.textView.frame =  CGRect(x:50,y:self.textView.frame.origin.y,width:self.textView.frame.size.width,height:self.textView.frame.size.height)
//        })
        scrolPager()
        SettingView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func SettingView() {

//        textView.text = "Touched Over 35 million lives"
//        UIView.animate(withDuration: 1.0, animations: { () -> Void in
//            self.textView.frame =  CGRect(x:50,y:self.textView.frame.origin.y,width:self.textView.frame.size.width,height:self.textView.frame.size.height)
//        })
        btnRegister.layer.cornerRadius = 10
        btnSignIn.layer.cornerRadius = 10
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
    }
    func scrolPager(){
        
        // Do any additional setup after loading the view, typically from a nib.
        self.scrollView.frame = CGRect(x:0, y:0, width:self.scrollView.frame.width, height:self.scrollView.frame.height)
        let scrollViewWidth:CGFloat = self.scrollView.frame.width
        let scrollViewHeight:CGFloat = self.scrollView.frame.height
        //2
        /*textView.textAlignment = .center
        textView.text = "Sweettutos.com is your blog of choice for Mobile tutorials"
        textView.textColor = .black
        self.startButton.layer.cornerRadius = 4.0
        //3
 */
        let imgOne = UIImageView(frame: CGRect(x:0, y:0,width:scrollViewWidth, height:scrollViewHeight))
        imgOne.image = UIImage(named: "splash1.jpg")
        let imgTwo = UIImageView(frame: CGRect(x:scrollViewWidth, y:0,width:scrollViewWidth, height:scrollViewHeight))
        imgTwo.image = UIImage(named: "splash2.jpg")
        let imgThree = UIImageView(frame: CGRect(x:scrollViewWidth*2, y:0,width:scrollViewWidth, height:scrollViewHeight))
        imgThree.image = UIImage(named: "splash3.jpg")
     
        
        self.scrollView.addSubview(imgOne)
        self.scrollView.addSubview(imgTwo)
        self.scrollView.addSubview(imgThree)
        //4
        self.scrollView.contentSize = CGSize(width:self.scrollView.frame.width * 3, height:self.scrollView.frame.height)
        self.scrollView.delegate = self
        //self.pageControl.currentPage = 0
        textView.text = "Touched Over 35 million lives"
        UIView.animate(withDuration: 1.0, animations: { () -> Void in
            self.textView.frame =  CGRect(x:50,y:self.textView.frame.origin.y,width:self.textView.frame.size.width,height:self.textView.frame.size.height)
        })
        
        
        //Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(moveToNextPage), userInfo: nil, repeats: true)
        
    }
    
    func moveToNextPage (){
       //  self.textView.frame =  CGRect(x:5000,y:self.textView.frame.origin.y,width:self.textView.frame.size.width,height:self.textView.frame.size.height)
        let pageWidth:CGFloat = self.scrollView.frame.width
        let maxWidth:CGFloat = pageWidth * 3
        let contentOffset:CGFloat = self.scrollView.contentOffset.x
        
        var slideToX = contentOffset + pageWidth
        
        if  contentOffset + pageWidth == maxWidth{
            slideToX = 0
        }
        self.scrollView.scrollRectToVisible(CGRect(x:slideToX, y:0, width:pageWidth, height:self.scrollView.frame.height), animated: true)
    }
    
    /*
    //MARK: UIScrollView Delegate
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView){
        // Test the offset and calculate the current page after scrolling ends
        let pageWidth:CGFloat = scrollView.frame.width
        let currentPage:CGFloat = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1
        // Change the indicator
        //self.pageControl.currentPage = Int(currentPage);
        // Change the text accordingly
        if Int(currentPage) == 0{
            self.pageControl.currentPage = Int(currentPage);

            textView.text = "Touched Over 35 million lives"
            UIView.animate(withDuration: 1.0, animations: { () -> Void in
                self.textView.frame =  CGRect(x:50,y:self.textView.frame.origin.y,width:self.textView.frame.size.width,height:self.textView.frame.size.height)
            })
        
        }else if Int(currentPage) == 1{
            self.pageControl.currentPage = Int(currentPage);

            textView.text = "5 million miracles and counting!"
            
            UIView.animate(withDuration: 1.0, animations: { () -> Void in
                self.textView.frame =  CGRect(x:50,y:self.textView.frame.origin.y,width:self.textView.frame.size.width,height:self.textView.frame.size.height)
            })
        }else if Int(currentPage) == 2{
            textView.text = "Proud Moments"
            self.pageControl.currentPage = Int(currentPage);

            UIView.animate(withDuration: 1.0, animations: { () -> Void in
                self.textView.frame =  CGRect(x:50,y:self.textView.frame.origin.y,width:self.textView.frame.size.width,height:self.textView.frame.size.height)
            })
        }
    }
    */
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let pageWidth:CGFloat = scrollView.frame.width
        let currentPage:CGFloat = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1
        // Change the indicator
        //self.pageControl.currentPage = Int(currentPage);
        // Change the text accordingly
        if Int(currentPage) == 0{
            print("00000")
            textView2.isHidden = true
            textView3.isHidden = true
            textView.isHidden = false
            self.pageControl.currentPage = Int(currentPage);
            
            textView.text = "Touched Over 35 million lives"
            UIView.animate(withDuration: 1.0, animations: { () -> Void in
                self.textView.frame =  CGRect(x:50,y:self.textView.frame.origin.y,width:self.textView.frame.size.width,height:self.textView.frame.size.height)
            })
        }else if Int(currentPage) == 1{
            print("000111100")
            textView2.isHidden = false
            textView3.isHidden = true
            textView.isHidden = true
            self.pageControl.currentPage = Int(currentPage);
            
            textView.text = "5 million miracles and counting!"
            
            UIView.animate(withDuration: 1.0, animations: { () -> Void in
                self.textView.frame =  CGRect(x:50,y:self.textView.frame.origin.y,width:self.textView.frame.size.width,height:self.textView.frame.size.height)
            })

        }else if Int(currentPage)==2{
            print("222200000")
            textView2.isHidden = true
            textView3.isHidden = false
            textView.isHidden = true
            textView.text = "Proud Moments"
            self.pageControl.currentPage = Int(currentPage);
            
            UIView.animate(withDuration: 1.0, animations: { () -> Void in
                self.textView.frame =  CGRect(x:50,y:self.textView.frame.origin.y,width:self.textView.frame.size.width,height:self.textView.frame.size.height)
            })

        }
        
    }
    
   

    @IBAction func actionLogin(_ sender:Any){
        let mainvc = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.present(mainvc, animated:true, completion:nil)
    }
 
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

