//
//  Header.h
//  Petnod
//
//  Created by admin on 2/7/17.
//  Copyright © 2017 admin. All rights reserved.
//

#ifndef Header_h
#define Header_h

#define SecreteKey ([NSString stringWithFormat :@"ApiReal0903"])
#define Domian_URL ([NSString stringWithFormat :@"http://expertteam.in"])

#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "Reachability.h"
#import "NSString+md5.h"
#import "SDImageCache.h"
#import "UIImageView+WebCache.h"
#import "SliderImageVC.h"
#import <GoogleSignIn/GoogleSignIn.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "iToast.h"
#import "SAMTextView.h"
#import "IQKeyboardManager.h"

#import "InitialSlidingViewController.h"
#import "MDSlidingViewController.h"
#import "ASStarRatingView.h"

#import "SWNinePatchImageFactory.h"
#import "SWNinePatchImageView.h"

#import "SWTableViewCell.h"
//#import "UIScrollView+APParallaxHeader.h"

#import "TestViewController.h"
#import "DoctorViewController.h"
#import "Masonry.h"
#import "StrechyParallaxScrollView.h"
#import "UUImageAvatarBrowser.h"
#import <UIImageView+UIActivityIndicatorForSDWebImage.h>


#import "MDGrowingTextView.h"
#import "TextViewInternal.h"
#import "Parse.h"

#import "FXBlurView.h"

#import "PaymentPageViewController.h"

//#import <Crashlytics/Crashlytics.h>

#endif /* Header_h */
