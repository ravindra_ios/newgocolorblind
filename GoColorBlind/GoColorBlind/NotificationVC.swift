//
//  NotificationVC.swift
//  GoColorBlind
//
//  Created by Prashant Shinde on 11/11/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit

class NotificationVC: UIViewController {

    @IBOutlet weak var notificationTV: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

            }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
      
    }
    @IBAction func onBackAction(_ sender: Any) {
         self.dismiss(animated: false, completion: nil)
    }
}

class NotificationCell : UITableViewCell
{
    @IBOutlet weak var notificationImg: UIImageView!
    @IBOutlet weak var descriptionLbl: UILabel!
}

extension NotificationVC : UITableViewDelegate, UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : NotificationCell = notificationTV.dequeueReusableCell(withIdentifier: "notificationcell") as! NotificationCell
        return cell
    }
}
