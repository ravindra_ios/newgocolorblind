//
//  PremiumMatchVC.swift
//  GoColorBlind
//
//  Created by Prashant Shinde on 11/11/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit

class PremiumMatchVC: UIViewController {

    @IBOutlet weak var tableViewOne: UITableView!
    @IBOutlet weak var navigationTitle: UINavigationBar!
    
    var Str : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if(Str == "one")
        {
            navigationTitle.topItem?.title = "Premium Matches"
        }
        else if(Str == "two")
        {
            navigationTitle.topItem?.title = "Recently Joined"
        }
        else
        {
            navigationTitle.topItem?.title = "Recently Visitors"
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func onBackAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
 
}
class CellOne : UITableViewCell
{
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var yearLbl: UILabel!
    @IBOutlet weak var languageLbl: UILabel!
    @IBOutlet weak var bachelorsLbl: UILabel!
    @IBOutlet weak var salaryLbl: UILabel!
    
}

extension PremiumMatchVC : UITableViewDataSource , UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CellOne = tableViewOne.dequeueReusableCell(withIdentifier: "cellone") as! CellOne
        return cell
    }
}
