//
//  EmptyTableViewCell.swift
//  GoColorBlind
//
//  Created by Prashant Shinde on 1/3/18.
//  Copyright © 2018 Prashant Shinde. All rights reserved.
//

import UIKit

class EmptyTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
